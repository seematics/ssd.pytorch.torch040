import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from layers import *
import os
from common.loading import reconciling_load

base = {
    '300': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'C', 512, 512, 512, 'M',
            512, 512, 512],
    '512': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'C', 512, 512, 512, 'M',
            512, 512, 512],
    '512l': [16, 16, 'M', 32, 32, 'M', 32, 32, 32, 'C', 32, 32, 32, 'M',
             32, 32, 32],
}
extras = {
    '300': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256, 128, 'S', 256],
    '512': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256, 128, 'S', 256, 128, 256],
}
mbox = {
    # number of priors per feature map location, per scale
    # The original implementation has 4 priors in the first and last 2 scales
    # and six in the rest.
    '300': [4, 6, 6, 6, 4, 4],
    # in 512 there is an extra scale.
    '512': [4, 6, 6, 6, 6, 4, 4],
}


class YSSD(nn.Module):
    """
    Y-shaped dual input SSD testing
    """
    def __init__(self, phase, size, base_l, base_r, extras, head, num_classes, cfg):
        super(YSSD, self).__init__()
        self.phase = phase
        self.num_classes = num_classes
        self.cfg = cfg
        self.size = size if isinstance(size, tuple) else (size, size)

        # SSD network
        self.feature_extractor_L = nn.ModuleList(base_l)
        self.feature_extractor_R = nn.ModuleList(base_r)
        # Layer learns to scale the l2 normalized features from conv4_3
        self.L2Norm_L = L2Norm(512, 20)

        self.extras_L = nn.ModuleList(extras)

        self.loc = nn.ModuleList(head[0])
        self.conf = nn.ModuleList(head[1])

        # create priors:
        # change cfg so that feature_maps hold the actual size of the feature maps
        self.priorbox = PriorBox(self.cfg)
        self.verify_priors_match_output = False
        # self.priors = Variable(self.priorbox.forward(), volatile=True)
        self.priors = None
        if phase == 'test':
            self.softmax = nn.Softmax(dim=-1)
            self.detect = Detect(num_classes=num_classes,
                                 bkg_label=cfg.get('background_label', 0),
                                 top_k=cfg.get('top_k', 200),
                                 conf_thresh=cfg.get('conf_thresh', 0.01),
                                 nms_thresh=cfg.get('nms_thresh', 0.45),
                                 cfg=cfg)

    # TODO - test dataparallel training with dual input, if not, need to do concat and split for single input
    def forward(self, left, right):
        """

        :param left: left input
        :param right: right input
        :return:
        """
        sources = list()
        loc = list()
        conf = list()

        # left - original route up to extra layers
        # TODO - make this applicable to general feature extractors
        # apply left feature extractor up to conv4_3 relu (very specific...)
        for k in range(23):
            left = self.feature_extractor_L[k](left)
        s_left = self.L2Norm_L(left)
        sources.append(s_left)
        # apply feature_extractor up to fc7

        STOP_BEFORE = len(self.feature_extractor_L) - 2
        for k in range(23, STOP_BEFORE):
            left = self.feature_extractor_L[k](left)

        # right - prepare the mix-in to the extra layers
        for k in range(STOP_BEFORE):
            right = self.feature_extractor_R[k](right)

        # mix-in left and right
        left = torch.cat([left, right], dim=1)

        # finish left
        for k in range(STOP_BEFORE, len(self.feature_extractor_L)):
            left = self.feature_extractor_L[k](left)
        sources.append(left)

        # apply extra layers and cache source layer outputs
        for k, v in enumerate(self.extras_L):
            left = F.relu(v(left), inplace=True)
            if k % 2 == 1:
                sources.append(left)
        """
        now we know the feature sizes, time to create the priors 
        right now since we just have a dual feature_extractor we only take the sources from LEFT
        """
        if self.priors is None:
            current_features = [tuple(source.shape[-2:]) for source in sources]
            self.priors = Variable(self.priorbox.forward(override_feature_sizes=current_features), volatile=True)
            self.verify_priors_match_output = True

            # need to push priors to cuda now?

        # apply multibox head to source layers - right now only take loc from left and conf from right
        for (merged, location, confidence) in zip(sources, self.loc, self.conf):
            loc.append(location(merged).permute(0, 2, 3, 1).contiguous())
            conf.append(confidence(merged).permute(0, 2, 3, 1).contiguous())

        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)
        # first time - test that number of priors matches loc and conf
        if self.verify_priors_match_output:
            self.priors.cuda()
            assert int(loc.shape[-1])/4 == int(self.priors.shape[0]), "Bug!"
            assert int(conf.shape[-1])/self.num_classes == int(self.priors.shape[0]), "Bug!"
            self.verify_priors_match_output = False

        if self.phase == "test":
            output = self.detect(
                loc.view(loc.size(0), -1, 4),                   # loc preds
                self.softmax(conf.view(conf.size(0), -1,
                                       self.num_classes)),                # conf preds
                self.priors.type(type(left.data))                  # default boxes
            )
        else:
            output = (
                loc.view(loc.size(0), -1, 4),
                conf.view(conf.size(0), -1, self.num_classes),
                self.priors
            )
        return output

    def _load_weights_to_both_components(self, base_file, init_func_on_skip=None, reload_from_similar=False,
                                         logger=None):
        prnt_func = print if logger is None else logger.console
        other, ext = os.path.splitext(base_file)
        if ext == '.pkl' or '.pth':
            prnt_func('Loading weights into state dict...')
            state_dict = torch.load(base_file, map_location=lambda storage, loc: storage)
            own_state_dict = self.state_dict()
            for suffix in ['_L', '_R', '']:
                for name, param in state_dict.items():
                    name_parts = name.split('.')
                    # compatabililty with vgg load
                    load_name_vgg_compat = (name_parts[0] == 'vgg')
                    name_parts[0] = name_parts[0] if not load_name_vgg_compat else 'feature_extractor'
                    name_parts[0] += suffix
                    load_name = '.'.join(name_parts)
                    if load_name in own_state_dict:
                        if param.size() != own_state_dict[load_name].size():
                            if reload_from_similar:
                                prnt_func(
                                    'For %s: name matches but size does not (mine: %s vs. other: %s)' % (
                                        name, str(param.size()), str(own_state_dict[load_name].size())))
                                raise NotImplemented
                            else:
                                if init_func_on_skip is not None:
                                    prnt_func('Cannot match %s because: load_size: %s net_size: %s' % (
                                        name, str(param.size()), str(own_state_dict[load_name].size())))
                                    if name_parts[-1] in ('weight', 'bias'):
                                        # prnt_func('Now reconciling with input parameters...')
                                        reconciling_load(own_state_dict[load_name], param, init_func=init_func_on_skip,
                                                         reconcile_tile_if_possible=False, logger=logger)
                                    else:
                                        # in place ( i think )
                                        prnt_func('Now initializing using user supplied function')
                                        init_func_on_skip(own_state_dict[load_name])
                                else:
                                    prnt_func('Skipping %s: load_size: %s net_size: %s' % (
                                        name, str(param.size()), str(own_state_dict[load_name].size())))
                                    continue
                        else:
                            own_state_dict[load_name].copy_(param)

                # self.load_state_dict(state_dict)
                prnt_func('Finished loading %s part!' % suffix.lstrip('_'))
        else:
            prnt_func('Sorry only .pth and .pkl files supported.')

    def load_with_skip(self, base_file, init_func_on_skip=None, reload_from_similar=False, logger=None):
        self._load_weights_to_both_components(base_file, init_func_on_skip=init_func_on_skip,
                                              reload_from_similar=reload_from_similar, logger=logger)

    def load_weights(self, base_file, logger=None):
        self._load_weights_to_both_components(base_file, init_func_on_skip=None,
                                              reload_from_similar=False, logger=logger)


# This function is derived from torchvision VGG make_layers()
# https://github.com/pytorch/vision/blob/master/torchvision/models/vgg.py
def vgg(cfg, i, batch_norm=False, conv7_in_dim=1024):
    layers = []
    in_channels = i
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        elif v == 'C':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    pool5 = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)
    conv6 = nn.Conv2d(512, 1024, kernel_size=3, padding=6, dilation=6)
    layers += [pool5, conv6,
               nn.ReLU(inplace=True)]
    if conv7_in_dim > 0:
        conv7 = nn.Conv2d(conv7_in_dim, 1024, kernel_size=1)
        layers += [conv7, nn.ReLU(inplace=True)]
    return layers


def thin_vgg(cfg, i, batch_norm=False, conv7_in_dim=1024):
    layers = []
    in_channels = i
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        elif v == 'C':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    pool5 = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)
    conv6 = nn.Conv2d(32, 64, kernel_size=3, padding=6, dilation=6)
    layers += [pool5, conv6,
               nn.ReLU(inplace=True)]
    return layers


def add_extras(cfg, i, batch_norm=False):
    # Extra layers added to VGG for feature scaling
    layers = []
    in_channels = i
    flag = False
    for k, v in enumerate(cfg):
        if in_channels != 'S':
            if v == 'S':
                layers += [nn.Conv2d(in_channels, cfg[k + 1],
                           kernel_size=(1, 3)[flag], stride=2, padding=1)]
            else:
                layers += [nn.Conv2d(in_channels, v, kernel_size=(1, 3)[flag])]
            flag = not flag
        in_channels = v
    return layers


def add_extras_512(cfg, i, hmap_channels=0):
    # Extra layers added to VGG for feature scaling
    layers = []
    add_hmap_chans = 1
    in_channels = i
    flag = False
    for k, v in enumerate(cfg[:-1]):
        if in_channels != 'S':
            if v == 'S':
                layers += [nn.Conv2d(in_channels, cfg[k + 1], kernel_size=(1, 3)[flag], stride=2, padding=1)]
            else:
                layers += [nn.Conv2d(in_channels + add_hmap_chans*hmap_channels, v, kernel_size=(1, 3)[flag])]
            flag = not flag
            add_hmap_chans = 0
        else:
            add_hmap_chans = 1
        in_channels = v

    layers += [nn.Conv2d(cfg[-2], cfg[-1], kernel_size=4, padding=1)]
    return layers


def multibox(vgg, extra_layers, cfg, num_classes, hmap_channels=0):
    loc_layers = []
    conf_layers = []
    vgg_source = [21, -2]
    for k, v in enumerate(vgg_source):
        loc_layers += [nn.Conv2d(vgg[v].out_channels + hmap_channels,
                                 cfg[k] * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(vgg[v].out_channels + hmap_channels,
                        cfg[k] * num_classes, kernel_size=3, padding=1)]
    for k, v in enumerate(extra_layers[1::2], 2):
        loc_layers += [nn.Conv2d(v.out_channels + hmap_channels, cfg[k]
                                 * 4, kernel_size=3, padding=1)]
        # TODO: add elementwise layer after conv2d to account for "heatmap"
        conf_layers += [nn.Conv2d(v.out_channels + hmap_channels, cfg[k]
                                  * num_classes, kernel_size=3, padding=1)]
    return vgg, extra_layers, (loc_layers, conf_layers)


def build_yssd(cfg, phase, variant=300, size=(300, 300), num_classes=21, left_input_dim=3, right_input_dim=3):
    if phase != "test" and phase != "train":
        print("ERROR: Phase: " + phase + " not recognized")
        return
    add_extras_func = {300: add_extras, 512: add_extras_512, '512l': add_extras_512}[variant]
    conv7_left_dim = {300: 2048, 512: 2048, '512l': (1024+64)}[variant]
    base_l, extras_l, head_l = multibox(vgg(base[str(variant)], left_input_dim, conv7_in_dim=conv7_left_dim),
                                        add_extras_func(extras[str(variant)], 1024),
                                        mbox[str(variant)], num_classes)
    if variant == '512l':
        base_r = thin_vgg(base['512l'], right_input_dim)
    else:
        base_r = vgg(base[str(variant)], 3, conv7_in_dim=0)

    return YSSD(phase, size, base_l, base_r, extras_l, head_l, num_classes=num_classes, cfg=cfg)


# def build_ssd_with_heatmap(cfg, phase, variant=300, size=(300, 300), num_classes=21, ssd_cls=SSD,
#                            extras_in_channels=1024, hmap_channels=0):
#     if phase != "test" and phase != "train":
#         print("ERROR: Phase: " + phase + " not recognized")
#         return
#     add_extras_func = {300: add_extras, 512: add_extras_512}[variant]
#     base_, extras_, head_ = multibox(vgg(base[str(variant)], 3, ),
#                                      add_extras_func(extras[str(variant)], extras_in_channels, hmap_channels),
#                                      mbox[str(variant)], num_classes, hmap_channels=hmap_channels)
#     return ssd_cls(phase, size, base_, extras_, head_, num_classes=num_classes, cfg=cfg)
