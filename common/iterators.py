from copy import deepcopy
from random import Random
import numpy as np

from allegroai import ImageFrame
from allegroai_api.services.v1_5.frames import Augmentation


# only for use on auto_annotate frames
def custom_iterator_crop_around_boxes_autoannotate(an_iterator, augmentation_strength_fact=1.0):
    print('DEBUG - Custom Iterator Auto annotate crop around boxes transform')
    # use iterator in order to map
    for frame in an_iterator:
        # if this is auto annotate, add crop around boxes transformation at the end
        if frame.meta and int(frame.meta.get('auto_annotate', 0)):
            for a in frame.augmentation:
                a.strength *= augmentation_strength_fact
            frame.augmentation.append(Augmentation(cls='custom', type='AugmentationCropAroundBoxes',
                                                   params=[0., 0., 0.], strength=1.0))
        yield frame


# helper function
def label_map_and_reject(roi, label_to_id_dict):
    label_id = [label_to_id_dict.get(l) for l in roi.label if label_to_id_dict.get(l)]
    if not label_id or -1 in label_id:
        # there is no mapping, what shall we do...
        # currently map to default ignore, we can also just remove the roi
        return None, -1
    else:
        # what do we do if we have multiple choice, we pick the lowest
        roi.label_num = sorted(label_id)[0]
        return roi, roi.label_num


def custom_iterator_mapping(an_iterator, label_to_id):
    print('DEBUG - Custom Iterator Mapping:', label_to_id)
    # use iterator in order to map
    for frame in an_iterator:
        # do the mapping
        # frame.hard_rois = [roi for roi in frame.rois if label_map_and_reject(roi, label_to_id)[1] == -2]
        frame.rois = [roi for roi in frame.rois if label_map_and_reject(roi, label_to_id)[1] != -1]
        # print('{} : {}'.format(frame.id, frame.src))
        yield frame


def custom_iterator_threshold_conf_level(an_iterator, thresh=0.0, drop_empty_frames=False):
    print('DEBUG - Custom Iterator Threshold Confidence Level: %.3f' % thresh)
    # use iterator in order to map
    for frame in an_iterator:
        # do the mapping
        frame.rois = [roi for roi in frame.rois if roi.confidence >= thresh]
        if drop_empty_frames and not frame.rois:
            continue
        yield frame


# mix between several iterators, resulting in an infinite iterator. clip length is used to match with iterators
# prepared with clip_iterator_from_images
def mix_iterators_infinite(list_iterators, list_probabilities, seed=1337, clip_length=31):
    assert len(list_iterators) == len(list_probabilities)
    left_over_frames = [None]*len(list_iterators)
    cumulative_prob = np.array(list_probabilities).cumsum()
    rng = Random(seed)
    while True:
        roll = rng.random() * cumulative_prob[-1]
        i = int(np.argmax(cumulative_prob > roll))
        selected_iterator = list_iterators[i]

        # continue from where we left
        if not left_over_frames[i]:
            left_over_frames[i] = next(selected_iterator)

        if not left_over_frames[i].key_frame:
            # no keyframe means it is not a video iterator - emulate clips
            for _ in range(clip_length):
                left_over_frames[i].key_frame = left_over_frames[i].id
                yield left_over_frames[i]
                left_over_frames[i] = next(selected_iterator)
        else:
            # get the entire clip (meaning until we switch the frame key.
            base_frame_key = left_over_frames[i].key_frame
            count = 0
            while left_over_frames[i].key_frame == base_frame_key:
                yield left_over_frames[i]
                count += 1
                left_over_frames[i] = next(selected_iterator)


# for "gifs", todo: make more efficient
def clip_iterator_from_images(finite_iterator, clip_length_odd=31, seed=1337, token_split_clips=None):
    """
    Clip iterator will generate clips (sequences) of length clip_length_odd from a finite iterator
    Note: the iterator turns a finite iterator to an infinite one
    :param finite_iterator: finite iterator, notice we first retrieve all the frames from the iterator
    :param clip_length_odd: clip length to get at the end (marked by frame.key_frame value)
    :param seed: seed number for the random choice of center key frame
    :param token_split_clips: split by src to dict and pick a clip from there
    """
    # Get all the frames here
    all_input_frames = [f for f in finite_iterator]
    all_input_frames.sort(key=lambda f: f.src)
    dict_clips = {}
    dict_clips_keys = []
    if token_split_clips is not None:
        for f in all_input_frames:
            k = f.src.split('/')[-token_split_clips]
            frame_list = dict_clips.get(k, [])
            frame_list.append(f)
            dict_clips[k] = frame_list
        dict_clips_keys = list(dict_clips.keys())
    clip_length_in_frames = 2 * (clip_length_odd // 2) + 1
    clip_radius = clip_length_in_frames // 2
    rnd = Random(seed)
    while True:
        if token_split_clips:
            # pick a clip
            clip_num = rnd.randint(0, len(dict_clips)-1)
            input_frames = dict_clips[dict_clips_keys[clip_num]]
        else:
            input_frames = all_input_frames
        key_frame_index = rnd.randint(clip_length_in_frames // 2,
                                      len(input_frames) - clip_length_in_frames // 2)
        # surround with clip_length
        base_frame = input_frames[key_frame_index]
        clip = input_frames[key_frame_index - clip_radius:key_frame_index + clip_radius + 1]
        for frame in clip:
            copy_f = deepcopy(frame)
            copy_f.key_frame = str(key_frame_index)
            copy_f.augmentation = base_frame.augmentation
            yield copy_f


# TODO - fix so we would not need this as a hack to remove post-augmentation boxes with zero area
def custom_iterator_threshold_roi_area(an_iterator, thresh=0.0):
    for frame in an_iterator:

        # roi.area not used... so can't do it this way:
        # frame.rois = [roi for roi in frame.rois if roi.area > thresh]

        areas = []
        for roi in frame.rois:
            box = ImageFrame.get_polygon_bounding_box(roi.poly)
            areas.append(np.prod(box[2:4] - box[0:2]))

        frame.rois = [roi for roi, area in zip(frame.rois, areas) if area > thresh]

        yield frame


def custom_iterator_fake_motion(an_iterator):
    for frame in an_iterator:
        motion_mat = np.zeros(frame.get_data().shape)

        yield (frame, motion_mat)


# This is what we would like to do for video or clips with fake key_frames
def match_augmentations_to_key_frame_iterator(iterator):
    prev_key_frame = None
    base_augmentation = None
    for frame in iterator:
        if frame.key_frame:
            if frame.key_frame != prev_key_frame:
                prev_key_frame = frame.key_frame
                base_augmentation = frame.augmentation
            frame.augmentation = base_augmentation
        yield frame
