import torch
import numpy as np
import time
import yaml

from argparse import ArgumentParser
from functools import partial
from logging import ERROR, WARNING
from pathlib2 import Path
from torch.autograd import Variable

from common.utils import ssd_output_to_allegro_format, setup_pytorch03, ImageSizeTuple, upload_model_snapshot, \
    resolve_resize_strategy
from common.utils import CyclicLRScheduler, freeze_first_n_layers, make_deterministic
from common.loading import default_weight_init_for_module
from common.utils import get_model_url, resolve_output_labels
from common.utils import bound_number_type, build_ssd
from common.visualization import DrawBoxesAndLabels
from common.batchers import make_ssd_batch

from allegroai.debugging.timer import Timer
from allegroai.imageframe import ImageFrame
from allegroai import DataView, Augmentation, DataPipe, Task, InputModel, OutputModel
from allegroai.dataview import IterationOrder
from allegroai.utilities.augmentations import CustomAugmentationZoom, CustomAugmentationFixedCropSize

from layers import DragonLoss
from scan_ssd_test import test_model

# =================   GLOBAL PARAMETERS  ==============================
# Help reuse task id when in development mode
TASK_NAME = 'Train SSD [scan mode] example'
PROJECT_NAME = 'pytorch ssd'

# epsilon for numerical methods:
EPS = np.finfo(float).eps

# Debug printing - set to true to report how many positive/negative examples were seen for that specific batch
DEBUG_REPORT_LOSS_INTERNALS = True

# for accurate timing, we reset the timers every 10 reports. (set to, e.g., -1 to disable this feature)
TIMERS_BENCHMARK_WINDOW = 10
AVERAGING_WINDOW = 100
# Debug timers
step_timer = Timer()
preprocess_timer = Timer()
convert_to_torch = Timer()
total_timer = Timer()


def get_parser(input_parser=None):
    parser = input_parser or ArgumentParser(
        description='Single Shot MultiBox Detector Training With Pytorch')

    # (0) Basic config
    parser.add_argument('--batch-size', default=6, type=int, help='Batch Size')
    parser.add_argument('--max-iterations', default=150000, type=int, help='Max number of iterations for train')
    parser.add_argument('--save-iterations', default=5000, type=int, help='Save model every K iters')
    parser.add_argument('--report-iterations', default=100, type=int, help='Report Iterations')
    parser.add_argument('--report-images-every-n-reports', default=3, type=int, help='Images report frequency')
    parser.add_argument('--conf-thresh-debug-images', default=0.35, help='Confidence threshold for debug images',
                        type=bound_number_type(minimum=0, maximum=1, dtype=float))
    parser.add_argument('--num-workers', default=32, type=int, help='Number of workers used for parallel data loading')
    parser.add_argument('--upload-destination', default='s3://allegro-examples', type=str,
                        help='Destination to upload debug images and models')

    # (1) Model control
    parser.add_argument('--feature-extraction-type', default='vgg16', type=str, help='Feature extraction network')
    parser.add_argument('--dropout', default=0.1, type=bound_number_type(minimum=0, maximum=1, dtype=float),
                        help='dropout value between 0 and 1')

    # (2) Testing control
    parser.add_argument('--test-size', default=2000, type=int, help='Number of iterations for test')
    parser.add_argument('--test-while-train', default=1, type=int, help='Run a test before training')
    parser.add_argument('--test-before-train', default=1, type=int, help='Run a test before training')

    # (3) pytorch specific
    parser.add_argument('--cuda', default=1, type=int, help='Use CUDA to train model')

    # (3.1) solver control (This task uses SGD)
    parser.add_argument('--end2end-train', default=0, type=int, help='Train full net architecture')
    parser.add_argument('--momentum', default=0.9, type=float, help='SGD momentum')
    parser.add_argument('--weight-decay', default=0.0005, type=float, help='Weight decay coefficient')

    # new additions, pending refactoring
    # resize strategy
    parser.add_argument('--resize-strategy', type=str, default='bigger_keep_ar',
                        help='In case of several sources per frame, only choose this source id to train on')

    # source id
    parser.add_argument('--source-id', type=str, default=None,
                        help='In case of several sources per frame, only choose this source id to train on')

    return parser


def train(start_iter, stop_iter):
    logger.console('... Train commence - getting iterator.')
    train_iterator = train_dataview.get_iterator()

    if args.source_id:
        source_id = [args.source_id]
    else:
        source_id = None
    resize_strategy = resolve_resize_strategy(args, logger)
    pipe = DataPipe(train_iterator, num_workers=args.num_workers,
                    frame_cls_kwargs=dict(target_width_height=target_image_size,
                                          resize_strategy=resize_strategy,
                                          default_source_ids_to_load=source_id))
    pipe_iterator = pipe.get_iterator()

    net.train()

    # loss counters
    cumulative_loc_loss_reported = []
    cumulative_conf_loss_reported = []

    batcher = partial(batcher_func, target_image_size_wh=target_image_size, source_id=args.source_id)

    iteration = start_iter

    logger.console('Got it. Training Start!')
    timing_histogram = None

    pre_batch_collect = []

    preprocess_timer.tic()
    total_timer.tic()
    for cnt, image_frame in enumerate(pipe_iterator):
        if iteration > stop_iter:
            break

        # collect the frames into a batch (i.e. a list of tuples)
        pre_batch_collect.append(image_frame)

        if len(pre_batch_collect) != args.batch_size:
            continue

        image_batch = pre_batch_collect
        pre_batch_collect = []
        batch = batcher(image_batch)

        preprocess_timer.toc()
        convert_to_torch.tic()

        batch_input = [torch.from_numpy(inp) for inp in batch.input]
        targets = [torch.from_numpy(t) for t in batch.targets]
        if len(batch.targets) != len(batch_input[0]):
            logger.console('ERROR - problem with batcher? targets should be the same size of input', level=ERROR)
            targets = [np.array([0.0001, 0.0001, 0.0001, 0.0001, 0]).reshape(1, 5) * batch.input[0].size]
            targets = [torch.from_numpy(t) for t in targets]

        batch_input = [Variable(inp.cuda()) if cuda_on else Variable(inp) for inp in batch_input]
        targets = [Variable(ann.cuda(), volatile=True) if cuda_on else Variable(ann, volatile=True) for ann in targets]

        convert_to_torch.toc()
        step_timer.tic()

        # forward
        out = net(*batch_input)

        optimizer.zero_grad()
        loss_l, loss_c = criterion(out, targets)
        loss = loss_l + loss_c

        # back-propagation
        loss.backward()

        # optimization
        lr_scheduler.step()
        optimizer.step()

        # we adopt a convention where the iteration count is "how many steps taken"
        iteration += 1

        step_timer.toc()

        # send statistics
        if iteration and iteration % args.report_iterations == 0:
            this_iter_loss = loss.item()
            this_iter_loss_l = loss_l.item()
            this_iter_loss_c = loss_c.item()
            if DEBUG_REPORT_LOSS_INTERNALS:
                logger.console('Iteration %-6d [DRAGON-LOSS] POS/NEG: %4d/%-4d Denominator: %d' %
                               (iteration, criterion.last_pos_num, criterion.last_neg_num, criterion.norm_coeff))

            # grab loss for this iteration
            if np.isfinite(this_iter_loss_l):
                cumulative_loc_loss_reported.append(this_iter_loss_l)
                cumulative_loc_loss_reported = cumulative_loc_loss_reported[-AVERAGING_WINDOW:]
            else:
                logger.console('NOTE: Localization loss was %f in iteration %d' % (this_iter_loss_l, iteration))
            if np.isfinite(this_iter_loss_c):
                cumulative_conf_loss_reported.append(this_iter_loss_c)
                cumulative_conf_loss_reported = cumulative_conf_loss_reported[-AVERAGING_WINDOW:]
            else:
                logger.console('NOTE: Classification loss was %f in iteration %d' % (this_iter_loss_c, iteration))

            # Scalar Reporting
            logger.report_scalar(title='Localization Loss', series='iter. mean', iteration=iteration,
                                 value=sum(cumulative_loc_loss_reported) / len(cumulative_loc_loss_reported))
            logger.report_scalar(title='Localization Loss', series='current', iteration=iteration,
                                 value=this_iter_loss_l)
            logger.report_scalar(title='Classification Loss', series='iter. mean', iteration=iteration,
                                 value=sum(cumulative_conf_loss_reported) / len(cumulative_conf_loss_reported))
            logger.report_scalar(title='Classification Loss', series='current', iteration=iteration,
                                 value=this_iter_loss_c)
            for idx, param_group in enumerate(optimizer.param_groups):
                logger.report_scalar(title='Learning-Rate', series='Param Group %d' % idx, iteration=iteration,
                                     value=param_group['lr'])

            # Note: iteration time reported as rates
            logger.report_scalar(title='Rates[1/sec]', series='images', iteration=iteration,
                                 value=(batch.size / total_timer.average_time))
            logger.report_scalar(title='Rates[1/sec]', series='iterations', iteration=iteration,
                                 value=(1 / total_timer.average_time))
            if TIMERS_BENCHMARK_WINDOW > 0:
                logger.report_scalar(title='Window Average Time (size %d)' % TIMERS_BENCHMARK_WINDOW,
                                     series='Iter time', iteration=iteration, value=total_timer.average_time)

            n_report = 7
            # draw histogram of timing last n*2 iterations
            # overhead time:
            overhead = total_timer.average_time - (
                    step_timer.average_time + convert_to_torch.average_time + preprocess_timer.average_time)
            this_histogram = np.atleast_2d(np.around(np.array(
                [preprocess_timer.average_time,
                 convert_to_torch.average_time,
                 step_timer.average_time,
                 overhead]) / batch.size, decimals=3)).T
            timing_histogram = np.hstack(
                (timing_histogram[:, -(n_report - 1):],
                 this_histogram)) if timing_histogram is not None else this_histogram
            histogram_labels = ['IO & pre', 'convert', 'step', 'overhead']
            if timing_histogram.shape[-1] >= n_report:
                logger.report_vector(title='Average Timing - last %d reports [sec/image]' % n_report, series='average',
                                     iteration=0, values=timing_histogram, labels=histogram_labels)
                timing_histogram = None

            logger.flush()

            # Image Reporting
            if (iteration // args.report_iterations) % args.report_images_every_n_reports == 0:
                t_images = time.time()
                net.eval()
                detections = net(*batch_input)
                detections = detections.data.cpu().numpy()
                all_batch_boxes, all_batch_scores = \
                    ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                                 conf_thresh=args.conf_thresh_debug_images,
                                                 img_width=target_image_size.w,
                                                 img_height=target_image_size.h)

                # PyTorch images are (3, h, w), for cv2 we need:
                #  (h, w, 3) transpose + RGB2BGR cycling
                img_to_draw = []
                for input_type in batch.input:
                    if input_type.shape[1] == 4:
                        first_three = input_type[:, :3, :, :]
                        first_three = first_three.transpose(0, 2, 3, 1)
                        img_to_draw.append(first_three)

                        last = input_type[:, 3:, :, :]
                        last = np.repeat(last, 3, axis=1)
                        last = last.transpose(0, 2, 3, 1)
                        img_to_draw.append(last)
                    elif input_type.shape[1] == 3:
                        np_images = input_type.transpose(0, 2, 3, 1)
                        img_to_draw.append(np_images)
                    else:
                        raise ValueError('Only 3 or 4 channels per input are supported ')

                data_name = ['Images']
                if len(data_name) < len(img_to_draw):
                    logger.console('Too many inputs, not designed for such amount, using default names', level=ERROR)
                    data_name.extend(['']*len(img_to_draw))
                for stage_id, np_images in enumerate(img_to_draw):
                    draws = draw_boxes_with_mapping(images=np_images, pred_scores=all_batch_scores,
                                                    pred_boxes=all_batch_boxes,
                                                    gt_boxes=batch.ground_truth)
                    for idx, img in enumerate(draws):
                        logger.report_image_and_upload(title='%s' % data_name[stage_id], series='img_%d' % idx,
                                                       iteration=iteration, matrix=img.astype(np.uint8))

                t_images = time.time() - t_images
                logger.console('Time to create debug images: %.4f (sec)' % t_images)
                logger.flush()
                net.train()

            # General log printouts
            msg = 'Iteration %-6d : t_iter %.3f (sec) | Loss %.4f' % \
                  (iteration, total_timer.average_time, this_iter_loss)
            logger.console(msg)
            if TIMERS_BENCHMARK_WINDOW > 0 and (
                    iteration // args.report_iterations) % TIMERS_BENCHMARK_WINDOW == 0:
                    logger.console('Resetting timer averages at iteration %d' % iteration)
                    step_timer.reset_average()
                    preprocess_timer.reset_average()
                    convert_to_torch.reset_average()
                    total_timer.reset_average()
        total_timer.toc()

        # store snapshot
        if iteration != 0 and iteration % args.save_iterations == 0:
            logger.console('Saving state, iter: %d' % iteration)
            upload_model_snapshot(net=ssd_net, model=output_model, iteration=iteration)
            if test_while_train:
                test_model(net, config_params, args, test_dataview,
                           test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
                           logger=logger, at_iteration=iteration, cuda_on=cuda_on)
                net.train()

        # Start timimg now so we do not add the test time to the average total time per step
        total_timer.tic()
        preprocess_timer.tic()

    # we are done, upload the last snapshot
    upload_model_snapshot(net=ssd_net, model=output_model, iteration=iteration)
    if test_while_train:
        test_model(net, config_params, args, test_dataview,
                   test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
                   logger=logger, at_iteration=iteration, cuda_on=cuda_on)
        net.train()
    logger.console('All done! Have a nice day!')
    task.mark_stopped()


if __name__ == '__main__':
    ###################################
    # Create task in allegro's system #
    ###################################
    task = Task.current_task(default_project_name=PROJECT_NAME, default_task_name=TASK_NAME)
    seed = task.get_random_seed()
    make_deterministic(seed)  # setup random seed from task for reproducibility

    #################################
    # Connect arguments to the task #
    #################################
    connect_to_task_parser = get_parser()
    task.connect(connect_to_task_parser)
    args = connect_to_task_parser.parse_args()
    if args.source_id == '':
        args.source_id = None

    logger = task.get_logger()
    logger.set_default_upload_destination(uri=args.upload_destination)
    logger.console('Running arguments: %s' % str(args))
    logger.flush()

    ##################################################
    # Defining input model connecting it to the task #
    ##################################################
    # Here we import a model from a url using import_model, since the user might not have a registered model yet
    # alternatively, if you already have a model id you could use this:
    # input_model = InputModel(model_id='12345')
    input_model_url, input_model_name = \
        get_model_url(args.feature_extraction_type)
    # Default labels for imported models that do not exist yet:
    input_label_enumeration =\
        {'hard': -2, 'ignore': -1, 'background': 0, 'aeroplane': 1, 'bicycle': 2, 'bird': 3, 'boat': 4, 'bottle': 5,
         'bus': 6, 'car': 7, 'cat': 8, 'chair': 9, 'cow': 10, 'diningtable': 11, 'dog': 12, 'horse': 13,
         'motorbike': 14, 'pottedplant': 15, 'person': 16, 'sheep': 17, 'sofa': 18, 'train': 19, 'tvmonitor': 20}

    input_model = InputModel.import_model(weights_url=input_model_url, name=input_model_name,
                                          design=None, label_enumeration=input_label_enumeration)
    input_model.publish()
    task.connect(input_model)

    #########################
    # Defining output model #
    #########################
    output_model = OutputModel(task=task, framework="PyTorch")
    output_model.set_upload_destination(uri=args.upload_destination)

    if not output_model.design:
        logger.console('Loading default design into output model')
        default_design_file = Path('scan_ssd_design.yaml')
        if not default_design_file.exists():
            raise IOError('Missing model design file in repo {}'.format(default_design_file))
        output_model.update_design(default_design_file.read_text())

    ######################################
    # Reading model design configuration #
    ######################################
    config_params = yaml.load(output_model.design)
    score_thresh = config_params.get('test_detection_conf_thresh', 0.5)
    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])

    #########################################################################################
    # Defining allegro's dataview to be used as the train set and connecting it to the task #
    #########################################################################################
    logger.console('Creating train dataview...')
    train_dataview = DataView(iteration_order=IterationOrder.random, iteration_infinite=True)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Car')
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Pedestrian', weight=2)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Person_sitting')
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Van', weight=2)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Cyclist', weight=2)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Truck', weight=2)

    # Any custom augmentations used must be registered as custom augmentations
    ImageFrame.register_custom_augmentation(operation_name='AugmentationZoomInOut',
                                            augmentation_class=CustomAugmentationZoom)
    # add custom augmentation (Zoom In/Out)
    ImageFrame.register_custom_augmentation(operation_name='FixedCropSize',
                                            augmentation_class=CustomAugmentationFixedCropSize)

    # Add Augmentation to train dataview
    aug_aff = Augmentation.Affine
    aug_pix = Augmentation.Pixel
    train_dataview.add_augmentation_affine(strength=1.0,
                                           operations=(aug_aff.bypass, aug_aff.reflect_horizontal))
    train_dataview.add_augmentation_affine(strength=1.5,
                                           operations=(aug_aff.bypass, aug_aff.rotate, aug_aff.shear, aug_aff.scale))
    train_dataview.add_augmentation_pixel(strength=1,
                                          operations=(aug_pix.bypass, aug_pix.blur, aug_pix.noise))
    train_dataview.add_augmentation_pixel(strength=1.5,
                                          operations=(aug_pix.bypass, aug_pix.recolor))
    train_dataview.add_augmentation_custom(operations=['FixedCropSize'],
                                           arguments={'FixedCropSize': {'width': target_image_size[0],
                                                                        'height': target_image_size[1]}})
    train_dataview.add_augmentation_custom(operations=['AugmentationZoomInOut'], strength=1.5)
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Person_sitting', to_label='Person')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Pedestrian', to_label='Person')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Cyclist', to_label='Person')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Van', to_label='Car')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Truck', to_label='Car')

    task.connect(train_dataview)

    ##################################################
    # Defining task's labels (class name to integer) #
    ##################################################
    script_labels = {'hard': -2, 'ignore': -1, 'background': 0,  'Car': 1, 'Person': 2}
    task_labels = task.get_labels_enumeration()

    output_model_labels = resolve_output_labels(task_labels, script_labels)

    output_model.update_labels(output_model_labels)
    train_dataview.set_labels(output_model_labels)  # Has no effect in remote mode

    #########################################
    # Adding auxiliary dataview as test set #
    #########################################
    test_dataview = DataView(iteration_order=IterationOrder.sequential, iteration_infinite=False)
    test_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='largely_occluded', to_label='hard')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Person_sitting', to_label='Person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Pedestrian', to_label='Person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Cyclist', to_label='Person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Van', to_label='Car')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Truck', to_label='Car')
    test_dataview.set_labels(output_model_labels)
    test_dataview_exists = task.connect_auxiliary('val', test_dataview)
    if test_dataview_exists:
        test_while_train = 1
    else:
        test_dataview = None
        logger.console('Auxiliary test dataview "val" was not found - deactivating test while train', level=ERROR)
        test_while_train = 0

    ######################################################
    # Apply mapping to the class that draws debug images #
    ######################################################
    id_to_label_mapping = {v: k for k, v in output_model_labels.items()}
    draw_boxes_with_mapping = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping, logger=logger)
    if test_while_train:
        label_to_id_mapping_for_test = test_dataview.get_labels()
        id_to_label_mapping_for_test = {v: k for k, v in label_to_id_mapping_for_test.items()}
        draw_boxes_with_mapping_for_test = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping_for_test,
                                                              logger=logger, score_threshold=score_thresh)

    #############################################################
    # Build the SSD net, adjusting for number of labels/classes #
    #############################################################
    # Adjust num-classes if there are more model labels than classes
    model_num_positive_classes = len([v for _, v in output_model_labels.items() if v > 0])
    design_num_classes = config_params.setdefault('num_classes', 1 + model_num_positive_classes)
    if design_num_classes != 1 + model_num_positive_classes:
        logger.console('Model design contained different number of classes than expected. Fixing to %d classes' % (
                    1 + model_num_positive_classes), WARNING)
        config_params['num_classes'] = 1 + model_num_positive_classes
        output_model.update_design(yaml.dump(config_params))
        logger.console('If you are purposely changing the number of classes, ignore this message', WARNING)
    else:
        logger.console('Now creating an SSD model for %d classes + bkg class' % model_num_positive_classes)

    ###########################################
    # Setting up Pytorch and building network #
    ###########################################
    logger.console('Setup pytorch...')
    cuda_on = setup_pytorch03(args=args, logger=logger, suppress_warnings=True)
    if cuda_on:
        logger.console('Getting device count:')
        device_count = torch.cuda.device_count()
        msg_device = '... There are %d devices ...' % device_count if device_count > 1 else '... Found 1 device ...'
        logger.console(msg_device)

    logger.console('Constructing the SSD net...')
    variant = 512 if max(target_image_size) >= 400 else 300
    ssd_net, freeze_n = build_ssd(args, config_params, variant, target_image_size)

    # wrap to enable training on multiple GPUs
    net = torch.nn.DataParallel(ssd_net) if cuda_on else ssd_net

    #############################
    # Configure loss and solver #
    #############################
    logger.console('... Done. Now configuring optimizer')
    if not args.end2end_train:
        # we freeze the feature extractor
        logger.console('end2end not set - assuming TRANSFER LEARNING, freezing first %d layers' % freeze_n)
        freeze_first_n_layers(ssd_net.features, freeze_n)

    params = filter(lambda p: p.requires_grad, net.parameters())

    optimizer = torch.optim.SGD(params, lr=config_params['lr_list'][0],
                                momentum=args.momentum, weight_decay=args.weight_decay)

    lr_scheduler = CyclicLRScheduler(optimizer=optimizer,
                                     upper_bound_steps=config_params['lr_steps'],
                                     lr_list=config_params['lr_list'])

    # Deprecation warning
    if 'min_number_of_background_priors' not in config_params:
        logger.warn('DragonLoss has changed - model design requires "min_number_of_background_priors" parameter')
        logger.warn('Default value of min_number_of_background_priors=100 will be used')
        config_params['min_number_of_background_priors'] = 100
        output_model.update_design(yaml.dump(config_params))

    if 'hard_negative_mix_in_background' not in config_params:
        logger.warn('DragonLoss has changed - model design requires "hard_negative_mix_in_background" parameter')
        logger.warn('Default value of hard_negative_mix_in_background=0.3 will be used')
        config_params['hard_negative_mix_in_background'] = 0.3
        output_model.update_design(yaml.dump(config_params))

    criterion = DragonLoss(num_classes=config_params['num_classes'],
                           overlap_thresh=config_params['multi_box_loss_overlap_thresh'],
                           prior_for_matching=True,
                           bkg_label=config_params['background_label'],
                           neg_mining=True,
                           neg_pos=config_params['negative_positive_ratio'],
                           neg_overlap=0.5,
                           encode_target=False,
                           variance=config_params['variance'],
                           min_bkg=config_params['min_number_of_background_priors'],
                           hard_neg_mix=config_params['hard_negative_mix_in_background'],
                           use_gpu=cuda_on)

    ###################################
    # Load input model weights to net #
    ###################################
    weights_file = input_model.get_weights()
    if weights_file is None:
        init_weight_function = partial(default_weight_init_for_module, method='xavier', logger=logger)
        logger.console('Initializing weights...')
        ssd_net.apply(init_weight_function)
    else:
        logger.console('Loading weights from: {}'.format(weights_file))
        ssd_net.forgiving_load(weights_file, logger=logger)

    ##################
    # Start training #
    ##################
    logger.console('... Done. Here we go!')
    batcher_func = make_ssd_batch
    if test_while_train and args.test_before_train:
        task.mark_started()
        test_model(net, config_params, args, test_dataview,
                   test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
                   logger=logger, at_iteration=0, cuda_on=cuda_on)

    start_at = 0
    stop_at = args.max_iterations
    # Set the net to training mode when done testing
    net.train()
    train(start_at, stop_at)
