# -*- coding: utf-8 -*-
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from ..box_utils import match, log_sum_exp


class DragonLoss(nn.Module):
    """SSD Weighted Loss Function
    Compute Targets:
        1) Produce Confidence Target Indices by matching  ground truth boxes
           with (default) 'priorboxes' that have jaccard index > threshold parameter
           (default threshold: 0.5).
        2) Produce localization target by 'encoding' variance into offsets of ground
           truth boxes and their matched  'priorboxes'.
        3) Hard negative mining to filter the excessive number of negative examples
           that comes with using a large number of default bounding boxes.
           (default negative:positive ratio 3:1)
    Objective Loss:
        L(x,c,l,g) = (Lconf(x, c) + αLloc(x,l,g)) / N
        Where, Lconf is the CrossEntropy Loss and Lloc is the SmoothL1 Loss
        weighted by α which is set to 1 by cross val.
        Args:
            c: class confidences,
            l: predicted boxes,
            g: ground truth boxes
            N: number of matched default boxes
        See: https://arxiv.org/pdf/1512.02325.pdf for more details.
    """

    def __init__(self, num_classes, overlap_thresh, prior_for_matching,
                 bkg_label, neg_mining, neg_pos, neg_overlap, encode_target, variance,
                 min_bkg=100, hard_neg_mix=0.3,
                 ignore_bg_in_gt=True, use_gpu=True):
        super(DragonLoss, self).__init__()
        self.use_gpu = use_gpu
        self.num_classes = num_classes
        self.threshold = overlap_thresh
        self.background_label = bkg_label
        self.encode_target = encode_target
        self.use_prior_for_matching = prior_for_matching
        self.do_neg_mining = neg_mining
        self.negpos_ratio = neg_pos
        self.neg_overlap = neg_overlap
        self.variance = variance
        # TODO: toggle whether the training incorporates ground truth "guidance" for background class
        if not ignore_bg_in_gt:
            raise NotImplemented('Incorporating ground-truth bkg is not yet available')
        self.ignore_bg_in_gt = ignore_bg_in_gt

        # Split all prirors into small, medium and large
        self._small_med_large = np.array([0.0, 0.52, 0.86, 1.0])

        # Number of sampled backgrounds per image
        # self._num_background = sampled_bkg

        # Minimum number of hard backgrounds
        # self._min_num_hard_bkg = min_hard_bkg
        self._min_num_bkg = min_bkg
        self._hard_neg_mix = hard_neg_mix

        self.last_pos_num = 0
        self.last_neg_num = 0
        self.norm_coeff = 0

        self._fw_count = 0
        self._av_pos = 0

    def sample_backgrounds(self, batch_size, num_priors, num_bkg):
        # TODO: some of these can be cached
        cutoffs = (self._small_med_large*num_priors).astype(np.int32)
        all_priors = np.arange(num_priors)
        # samples_per_bin = (np.diff(self._small_med_large)*self._num_background).astype(np.int32)

        bacth_bkg_indices = []
        bacth_bkg_indices_row = []
        for bidx in range(batch_size):
            samples_per_bin = (np.diff(self._small_med_large) * num_bkg.flatten().tolist()[bidx]).astype(np.int32)
            bkg_indices = []
            for bot, top, samples in zip(cutoffs[:-1], cutoffs[1:], samples_per_bin):
                bkg_indices += tuple(np.random.choice(all_priors[bot:top], samples, replace=False))

            major_indx = np.array(bkg_indices)
            bacth_bkg_indices.append(major_indx)
            bacth_bkg_indices_row.append(np.ones_like(major_indx)*bidx)

        bacth_bkg_indices = np.concatenate(bacth_bkg_indices)
        bacth_bkg_indices_row = np.concatenate(bacth_bkg_indices_row)
        neg_ind = np.zeros((batch_size, num_priors))
        neg_ind[(bacth_bkg_indices_row, bacth_bkg_indices)] = 1

        return Variable(torch.from_numpy(neg_ind) > 0)

    def forward(self, predictions, targets):
        """Multibox Loss
        Args:
            predictions (tuple): A tuple containing loc preds, conf preds,
            and prior boxes from SSD net.
                conf shape: torch.size(batch_size,num_priors,num_classes)
                loc shape: torch.size(batch_size,num_priors,4)
                priors shape: torch.size(num_priors,4)

            targets (tensor): Ground truth boxes and labels for a batch,
                shape: [batch_size,num_objs,5] (last idx is the label).
        """
        loc_data, conf_data, priors = predictions
        batch_size = loc_data.size(0)
        if len(priors.shape) > 2:
            priors = priors[0, :loc_data.size(1), :]
        else:
            priors = priors[:loc_data.size(1), :]
        num_priors = (priors.size(0))

        # match priors (default boxes) and ground truth boxes
        loc_trgt = torch.Tensor(batch_size, num_priors, 4)
        conf_trgt = torch.LongTensor(batch_size, num_priors)
        for idx in range(batch_size):
            gt_rel_boxes = targets[idx][:, :-1].data
            labels = targets[idx][:, -1].data
            defaults = priors.data
            loc, conf = match(self.threshold, gt_rel_boxes, defaults, self.variance, labels)
            loc_trgt[idx] = loc  # [num_priors,4] encoded offsets to learn
            conf_trgt[idx] = conf
        if self.use_gpu:
            loc_trgt = loc_trgt.cuda()
            conf_trgt = conf_trgt.cuda()
        # wrap targets
        # TODO pytorch 0.4 with torch.no_grad() (maybe?)
        loc_trgt = Variable(loc_trgt, requires_grad=False)
        conf_trgt = Variable(conf_trgt, requires_grad=False)

        pos = conf_trgt != self.background_label

        # Localization Loss (Smooth L1)
        # pos_idx is same as pos but with Shape: [batch,num_priors,4] - pos[k] = 1 ==> pos_idx[k, :] = [1, 1, 1, 1]
        pos_idx = pos.unsqueeze(pos.dim()).expand_as(loc_data)
        loc_p = loc_data[pos_idx].view(-1, 4)
        loc_trgt = loc_trgt[pos_idx].view(-1, 4)
        loss_l = F.smooth_l1_loss(loc_p, loc_trgt)

        # Compute max conf across batch for hard negative mining
        batch_conf = conf_data.view(-1, self.num_classes)
        # loss_c is ~ sum of logits for the wrong classes
        loss_c = log_sum_exp(batch_conf) - batch_conf.gather(1, conf_trgt.view(-1, 1))

        # Hard Negative Mining - this loss takes proposals with highest loss (regardless of bkg-fg)
        loss_c = loss_c.reshape(pos.shape)
        loss_c[pos] = 0  # filter out pos boxes for now
        loss_c = loss_c.view(batch_size, -1)
        _, loss_idx = loss_c.sort(1, descending=True)
        _, idx_rank = loss_idx.sort(1)
        num_pos = pos.long().sum(1, keepdim=True)
        num_total_neg = torch.clamp((self.negpos_ratio*num_pos).clamp(self._min_num_bkg), max=pos.size(1) - 1)
        num_hard_neg = torch.clamp((self._hard_neg_mix*num_total_neg.float()).long(), max=pos.size(1) - 1)
        num_easy_neg = num_total_neg - num_hard_neg
        hard_neg = idx_rank < num_hard_neg.expand_as(idx_rank)
        # num_neg = torch.clamp((self.negpos_ratio*num_pos).clamp(self._min_num_hard_bkg), max=pos.size(1) - 1)
        # neg = idx_rank < num_neg.expand_as(idx_rank)

        extra_neg = self.sample_backgrounds(batch_size=batch_size, num_priors=num_priors, num_bkg=num_easy_neg)
        if self.use_gpu:
            extra_neg = extra_neg.cuda()

        neg = (hard_neg + extra_neg).gt(0)
        # neg = (neg + extra_neg).gt(0)

        # Confidence Loss Including Positive and Negative Examples
        pos_idx = pos.unsqueeze(2).expand_as(conf_data)
        neg_idx = neg.unsqueeze(2).expand_as(conf_data)
        conf_p = conf_data[(pos_idx+neg_idx).gt(0)].view(-1, self.num_classes)
        targets_weighted = conf_trgt[(pos+neg).gt(0)]
        loss_c = F.cross_entropy(conf_p, targets_weighted)

        # Sum of losses: L(x,c,l,g) = (Lconf(x, c) + αLloc(x,l,g)) / N

        # number of positive boxes
        N_pos = pos.data.sum().type_as(loss_c)
        self.last_pos_num = N_pos
        # number of negative boxes
        N_neg = neg.data.sum().type_as(loss_c)
        self.last_neg_num = N_neg
        # Losses are per box (averaged)
        # The normalization coeff is used to normalize the importance of a batch compraed with other batches
        # Importance used to be determined by the number of positives in a batch
        normalization_coeff = N_pos if N_pos else (self._av_pos or max(N_neg / self.negpos_ratio, 1))
        self.norm_coeff = normalization_coeff
        loc_normalization_coeff = max(1, normalization_coeff)
        conf_normalization_coeff = max(1, normalization_coeff)

        if not loc_p.size():
            loss_l = Variable(torch.FloatTensor([0.]))
            if self.use_gpu:
                loss_l = loss_l.cuda()
        else:
            loss_l /= loc_normalization_coeff

        loss_c /= conf_normalization_coeff

        self._av_pos = (1. / (self._fw_count + 1))*N_pos + (self._fw_count / (self._fw_count + 1))*self._av_pos
        self._fw_count += 1

        return loss_l, loss_c
