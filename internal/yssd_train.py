import torch
import numpy as np
import cv2
import time
import yaml

from argparse import ArgumentParser
from collections import OrderedDict
from copy import copy
from functools import partial
from logging import ERROR
from pathlib2 import Path
from random import Random
from torch.autograd import Variable

from common.metrics import BoxesHistory, average_precision_from_curve
from common.utils import ssd_output_to_allegro_format, setup_pytorch03, ImageSizeTuple, upload_model_snapshot, \
    jitter_box_and_score
from common.utils import CyclicLRScheduler, freeze_first_n_layers
from common.loading import default_weight_init_for_module, xavier_init
from common.visualization import DrawBoxesAndLabels

from allegroai.dataview import IterationOrder, FilterByRoi
from allegroai.debugging.timer import Timer
from allegroai.imageframe import ResizeStrategy, ImageFrame
from allegroai import DataView, Affine, DataPipe, Task, Model
from allegroai.utilities.augmentations import CustomAugmentationZoom, \
    CustomAugmentationCropRois, CustomAugmentationOccludeRois, \
    CustomAugmentationCropAroundBoxes, CustomAugmentationEraseBorder

from common.iterators import clip_iterator_from_images, mix_iterators_infinite
from common.iterators import custom_iterator_mapping, custom_iterator_threshold_roi_area
from common.iterators import match_augmentations_to_key_frame_iterator, custom_iterator_threshold_conf_level
from common.motion_batchers import make_ssd_batch_with_motion

from layers import Detect
from layers.modules.dragon_loss import DragonLoss
from models.ssd import build_yssd


# Please see __main__ below.
# =================   GLOBAL NAMES - meant to be overridden by ui, help reuse task id  ==============================
TASK_NAME = 'Sample SSD train task'
PROJECT_NAME = 'Sample SSD'
# UPLOAD_DESTINATION = 's3://seematics-examples/netapp'
UPLOAD_DESTINATION = 's3://seematics-examples/models'
TASK_NAME = 'test motion stuff I'
PROJECT_NAME = 'ARIEL TESTS'

INPUT_MODEL_ID = '61d09722289f462fba4b3d79644408d9'
# Task Name	FullFrame of ReRUN fix Train CL3
# ^^^^

# other options:
# INPUT_MODEL_ID = 'baf0e3c14ffa4d509ffa9dca88d73f75'
# Train 480p - SAIVT_1600till1700 coco and voc - crop around boxes - 1:4 ratio - 0.85 conf_thr - 5000 iter

# ============ SAME HERE - But for the input model id ==========================
# This example starts with the following model:
# person_detector_480p_phase_I - CoCo - Voc - Peron - 480 p - Baseline
# on staging -
# INPUT_MODEL_ID = '2bacab1c60754d20a580176d423f5db9'
# person_detector_480p_phase_I - CoCo - Voc - Peron - 480 p - Baseline

# INPUT_MODEL_ID = 'd0e1de2a6af645bcb0ad2b52ce9b507f'
# saw some motion from MOT, im not sure if it is a good starting point but it is YSSD


# todo - collect to single dict and define later
step_timer = Timer()
preprocess_timer = Timer()
convert_to_torch = Timer()
total_timer = Timer()

EPS = np.finfo(float).eps

# motion normlization globals:
# normalized image diffs get this multiplier
RAMP = 240.0
# linear mixing for motion
ALPHA = 0.666

# every loss report, print also how many positive/negative examples were seen for that specific batch
DEBUG_REPORT_LOSS_INTERNALS = True


def normalize_motion(motion, ramp=1.0):
    motion_min = motion.min()
    motion_max = motion.max()
    motion = motion.astype(np.float32)
    if motion_max == motion_min and motion_min == 0:
        return motion
    motion_norm = (motion - motion_min) / (motion_max - motion_min + EPS)
    motion_norm *= ramp
    # cv2.imshow('test-motion', motion_norm)
    # cv2.waitKey(100)

    return motion_norm


def get_parser(input_parser=None):
    parser = input_parser or ArgumentParser(
        description='Single Shot MultiBox Detector Training With Pytorch')

    parser.add_argument('--contlearning', default=1, type=int, help='if true, means using a video input')
    # TODO - join these to group and make sure task.connect handles those correctly
    parser.add_argument('--num-workers', default=32, type=int, help='Number of workers used in dataloading')
    parser.add_argument('--batch-size', default=6, type=int, help='Batch Size')
    parser.add_argument('--save-iterations', default=5000, type=int, help='Save model every K iters')
    parser.add_argument('--report-iterations', default=100, type=int, help='Report Iterations')
    parser.add_argument('--report-images-every-n-reports', default=3, type=int, help='Images report frequency')
    parser.add_argument('--max-iterations', default=500000, type=int, help='Max number of iterations for train')

    # pytorch specific
    parser.add_argument('--cuda', default=1, type=int, help='Use CUDA to train model')
    # dragon loss
    parser.add_argument('--num-sampled-bkg', type=int, default=300, help='How many bkg samples to use with dragon loss')
    parser.add_argument('--min-hard-bkg', type=int, default=300, help='Min number of hard bkg (if no FG is present)')
    # mapping and dataview control
    parser.add_argument('--ignore-occluded', default=0, type=int, help='Map occluded objects to ignore')
    parser.add_argument('--roi-conf-thresh', default=0.65, type=float, help='Drop incoming rois under conf threshold')
    parser.add_argument('--dataviews-mix', help="Probability per dataview seperated by ','", default='0.6, .05, 0.35')
    # testing control
    parser.add_argument('--only-test', default=0, type=int, help='Only run the test code')
    parser.add_argument('--test-before-train', default=0, type=int, help='Run a test before training')
    parser.add_argument('--test-size', default=10000, type=int, help='Number of iterations for test')
    parser.add_argument('--hard-mapping-for-test', default=1, type=int, help='Map occluded objects to hard in test')

    # solver control
    parser.add_argument('--epoch-size', default=100000, type=int, help='Epoch size is for backward compat.')
    parser.add_argument('--momentum', default=0.9, type=float, help='SGD momentum')
    parser.add_argument('--weight-decay', default=0.0005, type=float, help='Weight decay coefficient')
    parser.add_argument('--end2end-train', default=0, type=int, help='Train full net architecture')

    # yssd only
    parser.add_argument('--right-input-ch', default=3, type=int, help='Number of color channels for right net input')
    return parser


def test_model(at_iteration=0, assume_seq_images=False, use_gt_map=False, use_inference_gt_map=False):
    # hard coded stuff:
    precision_recall_conf_list = [0.001, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 0.999]
    min_precision_for_integrated_metric = 0.7
    # Set net tyo eval mode for test run

    if args.test_size <= 0 or not test_dataview:
        logger.console('Skipping test, test_size=0')
        return

    net.eval()
    # Only one of use_gt_map and use_inference_gt_map can be true
    use_gt_map -= use_gt_map * use_inference_gt_map

    logger.console('%s' % str(80 * '-'))
    t_test = time.time()
    iou = [i for i in config_params['test_detection_iou_thresh']]
    # test model
    # not for motion:
    # batcher = partial(batcher_func, target_image_size_wh=target_image_size)
    batcher = partial(batcher_func, target_image_size_wh=target_image_size,
                      motion_channels=3 + 1 * (use_gt_map or use_inference_gt_map))
    # TODO - refactor iterator wrapping out of tset
    # Raw test data
    iterator = test_dataview.get_iterator()
    # Map labels to ID's
    mapped_iterator = custom_iterator_mapping(iterator, label_to_id=label_to_id_mapping_for_test)
    # Filter bad boxes
    mapped_filtered_iterator = custom_iterator_threshold_conf_level(mapped_iterator, thresh=args.roi_conf_thresh)
    # Make sure all boxes has area
    experiment_iterator = custom_iterator_threshold_roi_area(mapped_filtered_iterator, thresh=0.0)
    # wrap with clip-making iterators

    if not assume_seq_images:
        experiment_iterator = clip_iterator_from_images(finite_iterator=experiment_iterator, token_split_clips=3)

    # todo: test with bigger_keep_aspect_ratio?
    test_pipe = DataPipe(experiment_iterator,
                         frame_cls_kwargs=dict(target_width_height=target_image_size,
                                               resize_strategy=ResizeStrategy.ALWAYS_KEEP_ASPECT_RATIO))
    test_pipe_iterator = test_pipe.get_iterator()
    logger.console('Testing currently loaded weights...')

    box_history = BoxesHistory()

    pre_batch_collect = []
    last_key_frame = False
    prev_motion = None
    prev_image = None
    prev_gt_map = None
    cur_gt_map = None
    heat_gt_map = None
    cur_inf_map = None
    all_batch_boxes = None
    all_batch_scores = None
    flt_mean = 0
    report_iter = 0
    for test_iter, image_frame in enumerate(test_pipe_iterator):
        if test_iter > args.test_size:
            break

        # heatmaps
        if use_gt_map:
            cur_gt_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)
            for poly in image_frame.get_polygons():
                box = image_frame.get_polygon_bounding_box(poly['points']).astype(np.int)
                box, score = jitter_box_and_score(box, score=1.)
                cv2.rectangle(cur_gt_map, (box[0], box[1]), (box[2], box[3]), color=int(255 * score),
                              thickness=cv2.FILLED)

        elif use_inference_gt_map:
            cur_inf_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)
            if all_batch_boxes is not None and all_batch_boxes.size:
                last_idx = np.where(all_batch_boxes[:, 0] == all_batch_boxes[:, 0].max())[0]
                for seematics_box, score in zip(all_batch_boxes[last_idx, :], all_batch_scores[last_idx]):
                    box = seematics_box[1:5].astype(np.int)
                    box, score = jitter_box_and_score(box, score)
                    cv2.rectangle(cur_inf_map, (box[0], box[1]), (box[2], box[3]), color=int(255 * score),
                                  thickness=cv2.FILLED)

        # create the new motion matrix here
        # now check if we are the key frame (there is a field for that, called frame_key?!)
        if (assume_seq_images and test_iter == 0) or (
                not assume_seq_images and image_frame.get_meta_data().key_frame != last_key_frame):
            motion_mat = np.zeros_like(image_frame.get_data(), dtype=np.float32)
            prev_motion = motion_mat.copy()
            heat_gt_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)
            prev_image = image_frame.get_data().copy().astype(np.float32)
            last_key_frame = image_frame.get_meta_data().key_frame \
                if image_frame.get_meta_data().key_frame is not None else False
            if use_gt_map:
                prev_gt_map = cur_gt_map

            elif use_inference_gt_map:
                prev_gt_map = cur_inf_map
        else:
            # motion of the last frame time current motion
            # this also takes care of the case if the input images are slightly mismatching in shape
            # TODO - do this for heatmaps as well
            cur_image_flt = image_frame.get_data().astype(np.float32)
            h, w, _ = [min(dim) for dim in zip(cur_image_flt.shape, prev_image.shape)]
            motion_mat = cur_image_flt[:h, :w, :] - prev_image[:h, :w, :]
            h_m, w_m, _ = [min(dim) for dim in zip(motion_mat.shape, prev_motion.shape)]
            motion_mat = ALPHA * motion_mat[:h_m, :w_m, :] + (1.0 - ALPHA) * prev_motion[:h_m, :w_m, :]

            prev_image = cur_image_flt
            prev_motion = motion_mat.copy()

            if use_gt_map or use_inference_gt_map:
                gt_map_w = 0.6
                heat_gt_map = gt_map_w * heat_gt_map + (1 - gt_map_w) * prev_gt_map
                prev_gt_map = cur_gt_map if use_gt_map else cur_inf_map

        normed_motion = normalize_motion(motion_mat, ramp=RAMP)

        if use_gt_map or use_inference_gt_map:
            normed_motion = np.concatenate((normed_motion, np.atleast_3d(heat_gt_map).astype(np.float32)), axis=2)
        # not motion:
        # pre_batch_collect.append(image_frame)
        pre_batch_collect.append((image_frame, normed_motion))
        if len(pre_batch_collect) != args.batch_size:
            # yes we will lose the last incomplete batch size
            continue

        # collect the frames into a batch (i.e. a list of tuples)
        image_batch = pre_batch_collect
        pre_batch_collect = []
        # use the batch and zer o  it at the end

        # test batch
        batch = batcher(image_batch)
        batch_input = [torch.from_numpy(inp) for inp in batch.input]
        batch_input = [Variable(inp.cuda()) if cuda_on else Variable(inp) for inp in batch_input]

        # forward
        out = net(*batch_input)
        pred_probs = softmax(out[1])
        detections = decode_boxes(out[0], pred_probs, out[2])
        detections = detections.clamp(0., 1.)
        detections = detections.data.cpu().numpy()
        all_batch_boxes, all_batch_scores = \
            ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                         conf_thresh=min(precision_recall_conf_list),
                                         img_width=target_image_size.w,
                                         img_height=target_image_size.h)

        # Make sure we have hard rois
        all_ground_truth = tuple(bb for bb in (batch.ground_truth, batch.hard_ground_truth) if bb.size)
        if all_ground_truth:
            all_ground_truth = np.vstack(all_ground_truth)
            box_history.save_frame_data(pred_boxes=all_batch_boxes, pred_scores=all_batch_scores,
                                        gt_boxes=all_ground_truth)
        else:
            all_ground_truth = None

        report_iter += 1
        if report_iter and report_iter % args.report_iterations == 0:
            msg = 'Test iteration %-6d' % report_iter
            logger.console(msg)
        # TODO - fix this I dont know integer math
        if report_iter and report_iter % (args.report_iterations * args.report_images_every_n_reports) == 0:
            t_images = time.time()
            data_name = ['Images', 'Motion', 'GT-Map']
            img_to_draw = []
            for input_type in batch.input:
                if input_type.shape[1] == 4:
                    first_three = input_type[:, :3, :, :]
                    first_three = first_three.transpose(0, 2, 3, 1)
                    first_three = first_three[:, :, :, [2, 1, 0]]
                    img_to_draw.append(first_three)

                    last = input_type[:, 3:, :, :]
                    last = np.repeat(last, 3, axis=1)
                    last = last.transpose(0, 2, 3, 1)
                    last = last[:, :, :, [2, 1, 0]]
                    img_to_draw.append(last)
                elif input_type.shape[1] == 3:
                    np_images = input_type.transpose(0, 2, 3, 1)
                    np_images = np_images[:, :, :, [2, 1, 0]]
                    img_to_draw.append(np_images)
                else:
                    raise ValueError('Only 3,4 channels are supported')

            if len(data_name) < len(batch.input):
                logger.console('Too many inputs, not designed for such amount, using default names', level=ERROR)
                data_name.extend(['']*len(batch.input))
            for stage_id, np_images in enumerate(img_to_draw):
                draws = draw_boxes_with_mapping_for_test(images=np_images,
                                                         pred_scores=all_batch_scores,
                                                         pred_boxes=all_batch_boxes,
                                                         gt_boxes=all_ground_truth,
                                                         gt_boxes_labels=batch.ground_truth_labels,
                                                         gt_boxes_hard=batch.hard_ground_truth_boxes,
                                                         gt_boxes_hard_labels=batch.hard_ground_truth_labels)

                draws = [cv2.putText(im, 'TEST', (10, 30), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                         for im in draws]

                for idx, img in enumerate(draws):
                    logger.report_image_and_upload(title='Test - %s' % data_name[stage_id], series='test_img_%d' % idx,
                                                   iteration=report_iter, matrix=img.astype(np.uint8))

            t_images = time.time() - t_images
            logger.console('Iter %d Time to create test images: %.4f (sec)' % (report_iter, t_images))

    # Report results
    id_to_label_mapping = {v: k for k, v in reversed(label_to_id_mapping_for_test.items()) if v > 0}
    logger.console('Calculating precision and creating report...')

    for iou_thresh in iou:
        pr_curves_per_label = box_history.get_precision_recall_curve(
            iou_threshold=iou_thresh,
            conf_thresholds=precision_recall_conf_list,
            nms_threshold=config_params.get('nms_thres_for_metrics', 0.65))
        for label in pr_curves_per_label:
            pr_curve = pr_curves_per_label[label]
            label_str = id_to_label_mapping.get(label, 'Label %d' % label)
            ap = average_precision_from_curve(pr_curve)
            ap_with_min_prec = average_precision_from_curve(pr_curve, min_precision=min_precision_for_integrated_metric)
            simple_title = '{} | IOU=%3.2f' % iou_thresh
            prec_rec_title = '[{label_str}] Precision-Recall @IOU:{iou_thresh}'
            conf_f1_title = '[{label_str}] Conf-F1-score @IOU:{iou_thresh}'
            min_prec = min_precision_for_integrated_metric
            series_title = 'it: {at_iteration} ap: {ap:3.2f} nEmAP_{min_prec:3.2f}: {ap_with_min_prec:3.2f}'

            prec_recall_graph = [(recall, precision) for _, precision, recall in pr_curve.raw_curve]
            conf_fscore_graph = [(conf, ((2 * recall * precision) / (EPS + recall + precision)))
                                 for conf, precision, recall in pr_curve.raw_curve]
            top_fscore = max([x[1] for x in conf_fscore_graph])
            logger.console('AP=%.2f AP_with_min_prec=%.2f' % (ap, ap_with_min_prec))
            logger.report_scatter2d(
                labels=['conf_thr=%3.2f' % x[0] for x in conf_fscore_graph],
                title=prec_rec_title.format(**locals()),
                series=series_title.format(**locals()),
                iteration=-1,
                xaxis='Recall',
                yaxis='Precision',
                scatter=prec_recall_graph)

            logger.report_scatter2d(
                labels=['Recall=%3.2f, Precision=%3.2f' % (x[0], x[1]) for x in prec_recall_graph],
                title=conf_f1_title.format(**locals()),
                series=series_title.format(**locals()),
                iteration=-1,
                xaxis='Conf. Threshold',
                yaxis='F1-Score',
                scatter=conf_fscore_graph)

            logger.report_scalar(
                title=simple_title.format('Test results for %s' % label_str),
                series='Top F1-score',
                iteration=at_iteration,
                value=top_fscore)
            logger.report_scalar(
                title=simple_title.format('Test results for %s' % label_str),
                series='AP',
                iteration=at_iteration,
                value=ap)
            logger.report_scalar(
                title=simple_title.format('Test results for %s' % label_str),
                series='nEAP_%3.2f' % min_prec,
                iteration=at_iteration,
                value=ap_with_min_prec)

            logger.console(simple_title.format('Precision-Recall'))
            logger.console(prec_recall_graph)
            logger.console('-------------------------------------------------')
            logger.console(simple_title.format('Conf-F1-score'))
            logger.console(conf_fscore_graph)
            logger.console('-------------------------------------------------')
        logger.flush()

    logger.console('Finished testing snapshot!')
    logger.console('%s' % str(80 * '-'))
    msg = 'Iteration %-6d : test time %.3f (sec)' % (at_iteration, time.time() - t_test)
    logger.console(msg)
    logger.flush()
    # Set the net to training mode when done testing
    if not args.only_test:
        net.train()


def create_train_iterator(create_seq_clip_from_main_iterator=False, debug=False):
    # uses dataviews as global

    # Raw train data
    iterator = train_dataview.get_iterator()
    # extra images
    image_iterator = dataview_images.get_iterator()
    # background
    bkg_iterator = bkg_dataview.get_iterator()
    # wrap with clip-making iterator
    if create_seq_clip_from_main_iterator:
        # token_split_clips is hard coded for MOT images as motion
        iterator = clip_iterator_from_images(finite_iterator=iterator, token_split_clips=3)
    # Add images as aux data, make sure augmentations are copied
    # how to mix? take from args:
    probabilities_list = [float(x.strip()) for x in args.dataviews_mix.split(',')]
    iterator_list = [iterator, bkg_iterator, image_iterator]
    if len(iterator_list) != len(probabilities_list):
        raise IOError('Incorrect number of values in dataviews-mix, expeceting %d' % len(iterator_list))
    logger.console('iterator mix: %s' % '[iterator, background_iterator, image_iterator]')
    logger.console('with probabilities: %s' % str(probabilities_list))

    mixed_view_iterator = mix_iterators_infinite(iterator_list, probabilities_list,
                                                 seed=1337, clip_length=31)
    # make sure we copy keyframes
    train_iterator = match_augmentations_to_key_frame_iterator(mixed_view_iterator)
    # Map labels to ID's
    train_iterator = custom_iterator_mapping(train_iterator, label_to_id=label_to_id_mapping)
    # Filter boxes with low confidence if any
    train_iterator = custom_iterator_threshold_conf_level(train_iterator, thresh=args.roi_conf_thresh)
    # Make sure all boxes has area
    train_iterator = custom_iterator_threshold_roi_area(train_iterator, thresh=0.001)

    if debug:
        for frame in train_iterator:
            im = ImageFrame(frame).get_data()
            cv2.imshow('DEBUG TRAIN ITERATOR', im)
            cv2.waitKey(1)

    return train_iterator


def train(start_iter, stop_iter, assume_seq_images=False, use_gt_map=False, use_inference_gt_map=False):
    # Only one of use_gt_map and use_inference_gt_map can be true
    use_gt_map -= use_gt_map * use_inference_gt_map
    train_iterator = create_train_iterator(create_seq_clip_from_main_iterator=(not CLEARNING))
    pipe = DataPipe(train_iterator,
                    frame_cls_kwargs=dict(target_width_height=target_image_size,
                                          resize_strategy=ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO))
    pipe_iterator = pipe.get_iterator()

    # main training loop
    net.train()
    # loss counters
    cumulative_loc_loss_reported = 0
    cumulative_conf_loss_reported = 0
    # motion only
    batcher = partial(batcher_func,
                      target_image_size_wh=target_image_size,
                      motion_channels=3 + 1 * (use_gt_map or use_inference_gt_map))

    iteration = start_iter

    logger.console('Training Start!')
    timing_histogram = None

    pre_batch_collect = []
    last_key_frame = False
    prev_motion = None
    prev_image = None
    prev_gt_map = None
    cur_gt_map = None
    heat_gt_map = None
    cur_inf_map = None
    all_batch_boxes = None
    all_batch_scores = None

    preprocess_timer.tic()
    total_timer.tic()
    for cnt, image_frame in enumerate(pipe_iterator):
        if iteration > stop_iter:
            break

        # create the new ground truth map (for later use)
        if use_gt_map:
            cur_gt_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)
            for poly in image_frame.get_polygons():
                box = image_frame.get_polygon_bounding_box(poly['points']).astype(np.int)
                # TODO - use probability for ground truth
                box, score = jitter_box_and_score(box, score=1.)
                cv2.rectangle(cur_gt_map, (box[0], box[1]), (box[2], box[3]), color=int(255 * score),
                              thickness=cv2.FILLED)

        elif use_inference_gt_map:
            # TODO - check if this still working! - looks like all_batch_boxes is only updated once in a while!
            cur_inf_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)
            if all_batch_boxes is not None and all_batch_boxes.size:
                last_idx = np.where(all_batch_boxes[:, 0] == all_batch_boxes[:, 0].max())[0]
                for seematics_box, score in zip(all_batch_boxes[last_idx, :], all_batch_scores[last_idx]):
                    box = seematics_box[1:5].astype(np.int)
                    box, score = jitter_box_and_score(box, score)
                    cv2.rectangle(cur_inf_map, (box[0], box[1]), (box[2], box[3]), color=int(255 * score),
                                  thickness=cv2.FILLED)

        # create the new motion matrix here
        # now check if we are the key frame (there is a field for that, called frame_key?!)
        if (assume_seq_images and cnt == 0) or (
                not assume_seq_images and image_frame.get_meta_data().key_frame != last_key_frame):
            motion_mat = np.zeros_like(image_frame.get_data(), dtype=np.float32)
            prev_motion = motion_mat.copy()
            heat_gt_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)
            prev_image = image_frame.get_data().copy().astype(np.float32)
            last_key_frame = image_frame.get_meta_data().key_frame \
                if image_frame.get_meta_data().key_frame is not None else False

            if use_gt_map:
                heat_gt_map = cur_gt_map
                prev_gt_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)

            elif use_inference_gt_map:
                heat_gt_map = cur_inf_map
                prev_gt_map = np.zeros(shape=image_frame.get_data().shape[:2], dtype=np.float32)
        else:
            cur_image_flt = image_frame.get_data().astype(np.float32)
            # motion of the last frame time current motion
            h, w, _ = [min(dim) for dim in zip(cur_image_flt.shape, prev_image.shape)]
            motion_mat = cur_image_flt[:h, :w, :].astype(np.float32) - prev_image[:h, :w, :].astype(np.float32)
            h_m, w_m, _ = [min(dim) for dim in zip(motion_mat.shape, prev_motion.shape)]
            motion_mat = ALPHA * motion_mat[:h_m, :w_m, :] + (1.0 - ALPHA) * prev_motion[:h_m, :w_m, :]
            prev_image = cur_image_flt
            prev_motion = motion_mat.copy()

            if use_gt_map or use_inference_gt_map:
                gt_map_w = 0.6
                heat_gt_map = gt_map_w * heat_gt_map + (1 - gt_map_w) * prev_gt_map
                prev_gt_map = cur_gt_map if use_gt_map else cur_inf_map

        normed_motion = normalize_motion(motion_mat, ramp=RAMP)

        if use_gt_map or use_inference_gt_map:
            h, w, ch = motion_mat.shape
            motion_with_hmap = np.zeros((h, w, ch + 1))
            motion_with_hmap[:h, :w, :ch] = normed_motion[:h, :w, :h]
            motion_with_hmap[:h, :w, ch] = heat_gt_map[:h, :w]
            # normed_motion = np.concatenate((normed_motion, np.atleast_3d(heat_gt_map).astype(np.float32)), axis=2)
            normed_motion = motion_with_hmap

        # this is needed for the next section (croprois)
        # add crop around boxes on RGB Image, if we are using auto annotate
        if image_frame.get_polygons() and image_frame.get_meta_data().meta and \
                int(image_frame.get_meta_data().meta.get('auto_annotate', 0)):
            # to crop meta_images only if... e.g. not motion
            # we need to create a new augmentation instance so that the mask will be reset
            crop_out_transformer = CustomAugmentationCropAroundBoxes()
            im = crop_out_transformer.transform_image_data(image_frame._matrix_meta_image)

            # we want to crop-in only parts of the ROI but not the motion
            # rng = Random()
            # params = [rng.random() for _ in range(3)]
            # crop_in_transformer = CustomAugmentationCropRois(params=params, strength=0.5,
            #                                                  arguments=crop_roi_augment_config)
            #
            # im = crop_in_transformer.transform_image_data(CompatibilityWrapper(im, image_frame))

            # store it into ImageFrame Object
            normed_motion = (im, normed_motion)
        else:
            # we want to crop-in only parts of the ROI but not the motion
            rng = Random()
            params = [rng.random() for _ in range(3)]
            # crop_in_transformer = CustomAugmentationCropRois(params=params, strength=0.5,
            #                                                  arguments=crop_roi_augment_config)
            # im = crop_in_transformer.transform_image_data(image_frame._matrix_meta_image)
            # # store it into ImageFrame Object
            # normed_motion = (im, normed_motion)
            normed_motion = (image_frame.get_data(), normed_motion)

        # collect the frames into a batch (i.e. a list of tuples)
        pre_batch_collect.append((image_frame, normed_motion))

        if len(pre_batch_collect) != args.batch_size:
            # yes we will lose the last incomplete batch size
            continue

        image_batch = pre_batch_collect
        pre_batch_collect = []
        batch = batcher(image_batch)

        preprocess_timer.toc()
        convert_to_torch.tic()

        batch_input = [torch.from_numpy(inp) for inp in batch.input]
        targets = [torch.from_numpy(t) for t in batch.targets]
        if len(batch.targets) != len(batch_input[0]):
            logger.console('ERROR - problem with batcher? targets should be the same size of input', level=ERROR)
            targets = [np.array([0.0001, 0.0001, 0.0001, 0.0001, 0]).reshape(1, 5) * batch.input[0].size]
            targets = [torch.from_numpy(t) for t in targets]

        batch_input = [Variable(inp.cuda()) if cuda_on else Variable(inp) for inp in batch_input]
        # todo - fix for pytorch 0.4 inside the loss
        targets = [Variable(ann.cuda(), volatile=True) if cuda_on else Variable(ann, volatile=True) for ann in targets]

        convert_to_torch.toc()
        step_timer.tic()
        # forward
        out = net(*batch_input)

        optimizer.zero_grad()
        loss_l, loss_c = criterion(out, targets)
        loss = loss_l + loss_c
        # back-propagation
        loss.backward()
        # optimization
        lr_scheduler.step()
        optimizer.step()

        # we adopt a convention where the iteration count is "how many steps taken"
        iteration += 1

        step_timer.toc()

        # send statistics
        if iteration and iteration % args.report_iterations == 0:
            this_iter_loss = loss.item()
            this_iter_loss_l = loss_l.item()
            this_iter_loss_c = loss_c.item()
            if DEBUG_REPORT_LOSS_INTERNALS:
                logger.console('Iteration %-6d [DRAGON-LOSS] POS/NEG: %4d/%-4d Denominator: %d' %
                               (iteration, criterion.last_pos_num, criterion.last_neg_num, criterion.norm_coeff))
            # grab loss for this iteration
            if np.isfinite(this_iter_loss_l):  # todo - pytorch0.4 deprecated the [0] index for 0-dim tensor
                cumulative_loc_loss_reported += this_iter_loss_l
            else:
                logger.console('NOTE: Localization loss was %f in iteration %d' % (this_iter_loss_l, iteration))
            if np.isfinite(this_iter_loss_c):
                cumulative_conf_loss_reported += this_iter_loss_c
            else:
                logger.console('NOTE: Classification loss was %f in iteration %d' % (this_iter_loss_c, iteration))
            # Scalar Reporting
            logger.report_scalar(title='Localization Loss', series='iter. mean', iteration=iteration,
                                 value=cumulative_loc_loss_reported / float(iteration / args.report_iterations))
            logger.report_scalar(title='Localization Loss', series='current', iteration=iteration,
                                 value=this_iter_loss_l)
            logger.report_scalar(title='Classification Loss', series='iter. mean', iteration=iteration,
                                 value=cumulative_conf_loss_reported / float(iteration / args.report_iterations))
            logger.report_scalar(title='Classification Loss', series='current', iteration=iteration,
                                 value=this_iter_loss_c)
            for idx, param_group in enumerate(optimizer.param_groups):
                logger.report_scalar(title='Learning-Rate', series='Param Group %d' % idx, iteration=iteration,
                                     value=param_group['lr'])

            # Note: iteration time reported as rates
            logger.report_scalar(title='Rates[1/sec]', series='images', iteration=iteration,
                                 value=(batch.size / total_timer.average_time))
            logger.report_scalar(title='Rates[1/sec]', series='iterations', iteration=iteration,
                                 value=(1 / total_timer.average_time))

            n_report = 7
            # draw histogram of timing last n*2 iterations
            # overhead time:
            overhead = total_timer.average_time - (
                    step_timer.average_time + convert_to_torch.average_time + preprocess_timer.average_time)
            this_histogram = np.atleast_2d(np.around(np.array(
                [preprocess_timer.average_time,
                 convert_to_torch.average_time,
                 step_timer.average_time,
                 overhead]) / batch.size, decimals=3)).T
            timing_histogram = np.hstack(
                (timing_histogram[:, -(n_report - 1):],
                 this_histogram)) if timing_histogram is not None else this_histogram
            histogram_labels = ['IO & pre', 'convert', 'step', 'overhead']
            if timing_histogram.shape[-1] >= n_report:
                logger.report_vector(title='Average Timing - last %d reports [sec/image]' % n_report, series='average',
                                     iteration=0, values=timing_histogram, labels=histogram_labels)
                timing_histogram = None

            logger.flush()
            # Image Reporting
            if (iteration // args.report_iterations) % args.report_images_every_n_reports == 0:
                t_images = time.time()
                pred_probs = softmax(out[1])
                detections = decode_boxes(out[0], pred_probs, out[2])
                detections = detections.clamp(0., 1.)
                detections = detections.data.cpu().numpy()
                all_batch_boxes, all_batch_scores = \
                    ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                                 conf_thresh=config_params.get('train_conf_thresh_debug_images', 0.5),
                                                 img_width=target_image_size.w,
                                                 img_height=target_image_size.h)

                # PyTorch images are (3, h, w), for cv2 we need:
                #  (h, w, 3) transpose + RGB2BGR cycling
                img_to_draw = []
                for input_type in batch.input:
                    if input_type.shape[1] == 4:
                        first_three = input_type[:, :3, :, :]
                        first_three = first_three.transpose(0, 2, 3, 1)
                        img_to_draw.append(first_three)

                        last = input_type[:, 3:, :, :]
                        last = np.repeat(last, 3, axis=1)
                        last = last.transpose(0, 2, 3, 1)
                        img_to_draw.append(last)
                    elif input_type.shape[1] == 3:
                        np_images = input_type.transpose(0, 2, 3, 1)
                        img_to_draw.append(np_images)
                    else:
                        raise ValueError('Only 3 or 4 channels per input are supported ')

                data_name = ['Images', 'Motion', 'GT-Map']
                if len(data_name) < len(img_to_draw):
                    logger.console('Too many inputs, not designed for such amount, using default names', level=ERROR)
                    data_name.extend(['']*len(img_to_draw))
                for stage_id, np_images in enumerate(img_to_draw):
                    draws = draw_boxes_with_mapping(images=np_images, pred_scores=all_batch_scores,
                                                    pred_boxes=all_batch_boxes,
                                                    gt_boxes=batch.ground_truth)
                    for idx, img in enumerate(draws):
                        logger.report_image_and_upload(title='%s' % data_name[stage_id], series='img_%d' % idx,
                                                       iteration=iteration, matrix=img.astype(np.uint8))

                t_images = time.time() - t_images
                logger.console('Time to create debug images: %.4f (sec)' % t_images)

            # General log printouts
            msg = 'Iteration %-6d : t_iter %.3f (sec) | Loss %.4f' % \
                  (iteration, total_timer.average_time, this_iter_loss)
            logger.console(msg)
        total_timer.toc()
        # store snapshot, do not add the test time to the average total time per step
        if iteration != 0 and iteration % args.save_iterations == 0:
            logger.console('Saving state, iter: %d' % iteration)
            upload_model_snapshot(net=ssd_net, model=output_model, iteration=iteration)
            test_model(at_iteration=iteration, **heatmap_feature_test_kwargs)

        total_timer.tic()
        preprocess_timer.tic()

    # we are done, upload the last snapshot
    upload_model_snapshot(net=ssd_net, model=output_model, iteration=iteration)
    test_model(at_iteration=iteration, **heatmap_feature_test_kwargs)
    logger.console('All done! Have a nice day!')
    task.mark_stopped()


# this is the function that is run to decide what to do:
# note, right now everything that these functions need is in __main__ (global)
def runner(current_task):
    if args.test_before_train or args.only_test:
        current_task.mark_started()
        test_model(**heatmap_feature_test_kwargs)
    if not args.only_test:
        stop_iter = args.max_iterations
        start_iter = 0
        train(start_iter, stop_iter, **heatmap_feature_train_kwargs)


if __name__ == '__main__':
    connect_to_task_parser = get_parser()
    task = Task.current_task(default_project_name=PROJECT_NAME, default_task_name=TASK_NAME)
    logger = task.get_logger()
    logger.set_default_upload_destination(uri=UPLOAD_DESTINATION)
    logger.flush()
    task.connect(connect_to_task_parser)
    args = connect_to_task_parser.parse_args()

    logger.console('Running arguments: %s' % str(args))

    CLEARNING = True if args.contlearning else False

    # ========= (0) Define Custom Augmentations ============
    # register all custom augmentations
    # Roi cropping - needs some config
    ImageFrame.register_custom_augmentation(operation_name='CropROIsAugmentation',
                                            augmentation_class=CustomAugmentationCropRois)
    ImageFrame.register_custom_augmentation(operation_name='OccludeAugmentation',
                                            augmentation_class=CustomAugmentationOccludeRois)
    ImageFrame.register_custom_augmentation(operation_name='AugmentationZoomInOut',
                                            augmentation_class=CustomAugmentationZoom)
    ImageFrame.register_custom_augmentation(operation_name='EraseBordersAugmentation',
                                            augmentation_class=CustomAugmentationEraseBorder)
    crop_roi_augment_config = {
        'CropROIsAugmentation': {
            'rnd_mean': 0.5,  # gaussian mean
            'rnd_std': 0.67,  # gaussian width
            'min_percentage': 0.500,  # of resulting cropped-in area (not zeroed)
            'max_percentage': 0.293,  # of resutling cropped-in area (not zeroed)
            'max_object_size_to_process_in_percentage': 0.5,  # % of frame size
            'min_object_size_to_process_in_percentage': 0.1,  # % of frame size
            'adjust_box_after_crop': True
        }}

    occlude_roi_augment_config = {
        'OccludeAugmentation': {
            'rnd_mean': 0.5,  # gaussian mean
            'rnd_std': 0.67,  # gaussian width
            'min_percentage': 0.1,  # of resulting cropped-out area (zeroed)
            'max_percentage': 0.4,  # of resulting cropped-out area (zeroed)
            'max_object_size_to_process_in_percentage': 0.5,  # same as above
            'min_object_size_to_process_in_percentage': 0.05,  # same as above
        }}

    # ========== (1) Training Data ===========================
    train_dataview = DataView()
    if CLEARNING:
        train_dataview.add_query(dataset_name='^SAIVT',
                                 version_name='movie.*15.*to.*16.*CL1',
                                 conf_range=[args.roi_conf_thresh, 1.0])
        train_dataview.set_iteration_parameters(order=IterationOrder.random, infinite=True, sequence_minimum_time=300)
    else:
        train_dataview.add_query(dataset_name='^MOT',
                                 version_name='Import MOT17 training dataset version$')
        train_dataview.set_iteration_parameters(order=IterationOrder.random, infinite=False, sequence_minimum_time=300)

    # ========= (1.1) Add Augmentation to Train Data ===========================
    train_dataview.add_augmentation_affine(strength=1.0,
                                           operations=(Affine.bypass, Affine.reflect_horizontal))
    train_dataview.add_augmentation_affine(strength=1.5,
                                           operations=(Affine.bypass, Affine.rotate, Affine.shear, Affine.scale))
    train_dataview.add_augmentation_pixel(strength=1.5)

    # connect the dataview
    task.connect(train_dataview)

    # ========= (1.2) Background Data ========================
    bkg_dataview = DataView()
    bkg_dataview.add_query(dataset_name='^SAIVT', version_name='movie.*16.*to.*17.*baseline',
                           filter_by_roi=FilterByRoi.no_rois)

    bkg_dataview.add_augmentation_custom(operations=['EraseBordersAugmentation'])
    bkg_dataview.add_augmentation_affine(strength=1.0,
                                         operations=(Affine.bypass, Affine.reflect_horizontal))
    bkg_dataview.add_augmentation_affine(strength=1.5,
                                         operations=(Affine.bypass, Affine.rotate, Affine.shear, Affine.scale))
    bkg_dataview.add_augmentation_pixel(strength=1.5)
    bkg_dataview.set_iteration_parameters(order=IterationOrder.random, infinite=True)

    # ========= (2) Validation Data===========================
    test_dataview = DataView()
    test_dataview.add_query(dataset_name='^SAIVT', version_name='6269 Annotated Out Of 13268$')
    test_dataview.set_iteration_parameters(order=IterationOrder.sequential, infinite=False)

    # ========= (2.1) Images Data ============================
    dataview_images = DataView()
    dataview_images.add_query(dataset_name='COCO - Common Objects in Context', version_name='Train2017',
                              roi_query='person')
    # TODO - fix name for production
    dataview_images.add_query(dataset_name='PASCAL-VOC', version_name='Train-2012', roi_query='person')
    # ========= (2.1.1) Add Augmentation to Images Data ===========================
    dataview_images.add_augmentation_custom(operations=['AugmentationZoomInOut'], strength=1.67)
    # dataview_images.add_augmentation_custom(operations=['OccludeAugmentation'],
    #                                         arguments=occlude_roi_augment_config,
    # TODO - bug - does not work here
    # dataview_images.add_augmentation_custom(operations=['CropROIsAugmentation'],
    #                                         arguments=crop_roi_augment_config,
    #                                         strength=1.00)
    dataview_images.add_augmentation_affine(strength=1.0, operations=(Affine.bypass, Affine.reflect_horizontal))
    dataview_images.add_augmentation_affine(strength=1.5,
                                            operations=(Affine.bypass, Affine.rotate, Affine.shear, Affine.scale))
    dataview_images.add_augmentation_pixel(strength=1.5)
    dataview_images.set_iteration_parameters(order=IterationOrder.random, infinite=True)

    # ========= (3) Model Weights =============================
    input_model = Model.load_model(model_id=INPUT_MODEL_ID)
    task.connect_model(input_model)
    design_file = Path('yssd_design.yaml')
    if not design_file.exists():
        raise IOError('Missing model design file in repo {}'.format(design_file))
    output_model = Model(task=task)
    output_model.set_upload_destination(uri='s3://seematics-examples/bosch')
    output_model.update_design(design_file.read_text())
    output_model.connect(task=task)

    # ========= (4) Mapping ==============================

    # ========= (4.1) Train mapping ==============================
    # TODO - debug this line
    # model_id_to_label_mapping = \
    #     {v: k for k, v in input_model.get_labels().items()} or {0: '__background__', 1: 'person'}
    model_id_to_label_mapping = {0: '__background__', 1: 'person'}
    # reverse
    label_to_id_mapping = {v: k for k, v in model_id_to_label_mapping.items()}
    map_to_person = OrderedDict({'person': 1, 'Pedestrian': 1, 'Person on vehicle': 1, 'Static person': 1})
    # make possible to train on both MOT and SAIVT
    label_to_id_mapping = OrderedDict(list(label_to_id_mapping.items()) + list(map_to_person.items()))
    # we want to ignore rois with these labels
    ignore_rois_with_labels = {'HeadTop': -1, 'Neck': -1, '__background__': -1, 'background': -1}
    ignore_rois_with_labels.update({'largely_obstructed': -1})
    if args.ignore_occluded:
        # for SAIVT
        ignore_rois_with_labels.update({'HeadOccluded': -1, 'Occluded': -1})
        # for MOT
        ignore_rois_with_labels.update({'evaluation_ignored': -1})
    label_to_id_mapping = OrderedDict(list(label_to_id_mapping.items()) + list(ignore_rois_with_labels.items()))

    # ========= (4.2) Validation mapping ==============================
    hard_mapping_for_test = {'largely_obstructed': -2, 'HeadOccluded': -2, 'Occluded': -2}
    if args.hard_mapping_for_test:
        label_to_id_mapping_for_test = \
            OrderedDict(list(label_to_id_mapping.items()) + list(hard_mapping_for_test.items()))
    else:
        label_to_id_mapping_for_test = copy(label_to_id_mapping)
    test_dataview.label_to_id_mapping = label_to_id_mapping_for_test

    # Drawing tool with mapping
    # initialize debug drawing with mapping
    id_to_label_mapping = {v: k for k, v in reversed(label_to_id_mapping.items()) if v > 0}
    # all -2 are drawn as __hard__
    id_to_label_mapping.update({-2: '__hard__'})
    # all -1 are drawn as __ignore__
    id_to_label_mapping.update({-1: '__ignore__'})
    # all 0 are drawn as __ignore__
    id_to_label_mapping.update({0: '__background__'})
    draw_boxes_with_mapping = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping, logger=logger)
    # TODO - write a setter for drawboxes so that we can update the threshold
    # HACK - so we wont have millions of boxes we use test_detection_conf_thresh as a score thresh for drawing boxes
    config_params = yaml.load(output_model.get_design())
    score_thresh = config_params.get('test_detection_conf_thresh', 0.5)
    draw_boxes_with_mapping_for_test = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping, logger=logger,
                                                          score_threshold=score_thresh)

    # good place for debug
    # if True:
    #     _ = create_train_iterator(create_seq_clip_from_main_iterator=True, debug=True)

    # ========= (5) Data Iterator ============================
    """ Iterators are defined inside train/test functions """

    # ========= (6) Build Model ==============================
    config_params = yaml.load(output_model.get_design())

    cuda_on = setup_pytorch03(args=args, logger=logger, suppress_warnings=False)
    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])

    ssd_net = build_yssd(cfg=config_params, phase='train',
                         variant=config_params['variant'], size=target_image_size,
                         num_classes=config_params['num_classes'], right_input_dim=args.right_input_ch)

    # wrap to enable on multiple gpus
    net = torch.nn.DataParallel(ssd_net) if cuda_on else ssd_net

    decode_boxes = Detect(num_classes=config_params['num_classes'], bkg_label=0,
                          top_k=config_params.get('top_k', 200),
                          conf_thresh=config_params.get('conf_thresh', 0.01),
                          nms_thresh=config_params['nms_thresh'], cfg=config_params)
    softmax = torch.nn.Softmax(dim=-1)

    # This is a Freeze for Y-ssd
    # for ssd you need to use ssd_net.vgg
    if not args.end2end_train:
        # we freeze the left feature extractor
        freeze_l = 33
        freeze_r = 21
        # freeze_l = freeze_r = 21
        freeze_first_n_layers(ssd_net.feature_extractor_L, freeze_l)
        if config_params['variant'] != '512l':
            freeze_first_n_layers(ssd_net.feature_extractor_R, freeze_r)

    params = filter(lambda p: p.requires_grad, net.parameters())

    optimizer = torch.optim.SGD(params, lr=config_params['lr_list'][0],
                                momentum=args.momentum, weight_decay=args.weight_decay)

    lr_scheduler = CyclicLRScheduler(optimizer=optimizer,
                                     upper_bound_steps=config_params['lr_steps'],
                                     lr_list=config_params['lr_list'])

    criterion = DragonLoss(num_classes=config_params['num_classes'],
                           prior_for_matching=True,
                           encode_target=False,
                           overlap_thresh=config_params['multi_box_loss_overlap_thresh'],
                           sampled_bkg=args.num_sampled_bkg,
                           min_hard_bkg=args.min_hard_bkg,
                           neg_mining=True,
                           bkg_label=config_params['background_label'],
                           neg_pos=config_params['negative_positive_ratio'],
                           neg_overlap=0.5,
                           variance=config_params['variance'],
                           use_gpu=cuda_on)

    # ======== (7) Load Weights ===================

    weights_file = input_model.get_weights()
    init_weight_function = partial(default_weight_init_for_module, method='xavier', logger=logger)
    if weights_file is None:
        # initialize all layers weights with xavier method
        logger.console('Initializing weights...')
        ssd_net.apply(init_weight_function)
    else:
        logger.console('Resuming training, loading {}...'.format(weights_file))
        ssd_net.forgiving_load(weights_file, init_func_on_skip=xavier_init, logger=logger)

    # ======== (8) Batching logic ===================
    batcher_func = make_ssd_batch_with_motion
    heatmap_feature_train_kwargs = {'use_inference_gt_map': False, 'use_gt_map': False, 'assume_seq_images': False}
    heatmap_feature_test_kwargs = {'use_inference_gt_map': False, 'use_gt_map': False, 'assume_seq_images': True}

    runner(current_task=task)
