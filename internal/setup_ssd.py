from collections import namedtuple

import numpy as np
import torch
import torch.backends.cudnn as cudnn
from torch.autograd import Variable

from common.utils import CyclicLRScheduler, freeze_first_n_layers
from models.ssd import build_ssd, build_yssd, MultiBoxLoss, Detect, SSD, build_ssd_with_heatmap

import torch.nn.functional as F


def build_net(cfg, args, input_image_size, use_cudnn=True, training=True, right_in_ch=3):
    which_net = cfg.get('feature', None)
    if which_net != 'yssd':
        ssd_net = build_ssd(cfg=cfg,
                            phase='train',
                            variant=cfg['variant'],
                            size=input_image_size,
                            num_classes=cfg['num_classes'])
    else:
        ssd_net = build_yssd(cfg=cfg,
                             phase='train',
                             variant=cfg['variant'],
                             size=input_image_size,
                             num_classes=cfg['num_classes'],
                             right_input_dim=right_in_ch)

    # wrap to enable on multiple gpus
    net = torch.nn.DataParallel(ssd_net) if use_cudnn else ssd_net

    decode_boxes = Detect(num_classes=cfg['num_classes'],
                          bkg_label=0, top_k=cfg.get('top_k', 200),
                          conf_thresh=cfg.get('conf_thresh', 0.01),
                          nms_thresh=cfg['nms_thresh'], cfg=cfg)

    softmax = torch.nn.Softmax(dim=-1)

    if training:
        # TODO - should move from cfg to exec params
        end_to_end = cfg.get('end_to_end', False)
        # we spcifically state require_grad = False for bottom vgg layers
        if not end_to_end:
            if which_net != 'yssd':
                freeze_first_n_layers(ssd_net.vgg, 21)
            else:
                freeze_first_n_layers(ssd_net.feature_extractor_L, 21)
                freeze_first_n_layers(ssd_net.feature_extractor_R, 21)

        params = filter(lambda p: p.requires_grad, net.parameters())

        optimizer = torch.optim.SGD(params, lr=cfg['lr_list'][0], momentum=args.momentum,
                                    weight_decay=args.weight_decay)

        lr_scheduler = CyclicLRScheduler(optimizer=optimizer, upper_bound_steps=cfg['lr_steps'], lr_list=cfg['lr_list'])
    else:
        optimizer = None
        lr_scheduler = None

    criterion = MultiBoxLoss(num_classes=cfg['num_classes'],
                             overlap_thresh=cfg['multi_box_loss_overlap_thresh'], prior_for_matching=True, bkg_label=cfg['background_label'],
                             neg_mining=True, neg_pos=cfg['negative_positive_ratio'], neg_overlap=0.5,
                             encode_target=False, variance=cfg['variance'], use_gpu=use_cudnn)

    if not training:
        ssd_net.eval()

    net_components = namedtuple('SSDComponents', ['ssd', 'data_parallel', 'decode_boxes', 'softmax', 'optimizer', 'lr_scheduler', 'loss'])

    return net_components(ssd_net, net, decode_boxes, softmax, optimizer, lr_scheduler, criterion)


def pool2d_to_size(source, target, dims=(2, 3), kernel=(5, 5), max_pool=True):
    strides = tuple(int(np.ceil(source.size(i) / target.size(i))) for i in dims)

    if max_pool:
        pooled_source = F.max_pool2d(source, kernel, strides)
    else:
        pooled_source = F.avg_pool2d(source, kernel, strides)

    # dim diffs after max pool
    pads = tuple(target.size(d) - pooled_source.size(d) for d in range(pooled_source.dim()))
    assert all((pads[d] >= 0 for d in dims)), 'Pooled source larger than target, how can it be? Pooled: %s, Target: %s' % (pooled_source.size(), target.size())
    pads = (0, pads[3], 0, pads[2])
    pooled_source = F.pad(pooled_source, pads)

    return pooled_source


class SSDWithObjectHeatmaps(SSD):

    def forward(self, x, heatmaps):
        """
        See parent for full documenmtation
        :param heatmaps: autograd.Variable[float32] (batch_size, 1, h, w) of binary heatmaps
        """
        sources = list()
        loc = list()
        conf = list()

        max_pool = True

        # apply vgg up to conv4_3 relu
        for k in range(23):
            x = self.vgg[k](x)

        s = self.L2Norm(x)
        hmap = pool2d_to_size(heatmaps, s, max_pool=max_pool)
        s = torch.cat([s, hmap], dim=1)
        sources.append(s)

        # apply vgg up to fc7
        for k in range(23, len(self.vgg)):
            x = self.vgg[k](x)

        hmap = pool2d_to_size(heatmaps, x, max_pool=max_pool)
        x = torch.cat([x, hmap], dim=1)
        sources.append(x)

        # apply extra layers and cache source layer outputs
        for k, v in enumerate(self.extras):
            x = F.relu(v(x), inplace=True)
            if k % 2 == 1:
                hmap = pool2d_to_size(heatmaps, x, max_pool=max_pool)
                x = torch.cat([x, hmap], dim=1)
                sources.append(x)
        """
        now we know the feature sizes, time to create the priors 
        """
        if self.priors is None:
            current_features = [tuple(source.shape[-2:]) for source in sources]
            self.priors = Variable(self.priorbox.forward(override_feature_sizes=current_features), volatile=True)
            self.verify_priors_match_output = True

            # need to push priors to cuda now?

        # apply multibox head to source layers
        for (x, l, c) in zip(sources, self.loc, self.conf):
            loc.append(l(x).permute(0, 2, 3, 1).contiguous())
            conf.append(c(x).permute(0, 2, 3, 1).contiguous())

        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)
        # first time - test that number of priors matches loc and conf
        if self.verify_priors_match_output:
            self.priors.cuda()
            assert int(loc.shape[-1]) / 4 == int(self.priors.shape[0]), "Bug!"
            assert int(conf.shape[-1]) / self.num_classes == int(self.priors.shape[0]), "Bug!"
            self.verify_priors_match_output = False

        if self.phase == "test":
            output = self.detect(
                loc.view(loc.size(0), -1, 4),  # loc preds
                self.softmax(conf.view(conf.size(0), -1,
                                       self.num_classes)),  # conf preds
                self.priors.type(type(x.data))  # default boxes
            )
        else:
            output = (
                loc.view(loc.size(0), -1, 4),
                conf.view(conf.size(0), -1, self.num_classes),
                self.priors
            )
        return output


# SSD class, number of channels in a heatmap
SSD_designs = {'ssd': (SSD, 0), 'heatmap': (SSDWithObjectHeatmaps, 1)}


def build_net_with_heatmap(cfg, args, use_cudnn=True, training=True, net_design='ssd'):
    ssd_cls, hmap_channels = SSD_designs.get(net_design, 'ssd')
    ssd_net = build_ssd_with_heatmap(cfg=cfg, phase='train', variant=cfg['variant'], size=(cfg['min_dim_w'], cfg['min_dim_h']), num_classes=cfg['num_classes'],
                                     ssd_cls=ssd_cls, hmap_channels=hmap_channels)
    net = ssd_net
    if use_cudnn:
        net = torch.nn.DataParallel(ssd_net)
        cudnn.benchmark = True

    # TODO - unify defaults and decide what to take from cfg and what from exec params
    decode_boxes = Detect(num_classes=cfg['num_classes'],
                          bkg_label=0, top_k=cfg.get('top_k', 200),
                          conf_thresh=cfg.get('conf_thresh', 0.01),
                          nms_thresh=cfg['nms_thresh'], cfg=cfg)

    softmax = torch.nn.Softmax(dim=-1)

    if training:
        # TODO - should move from cfg to exec params
        end_to_end = cfg.get('end_to_end', False)
        # we spcifically state require_grad = False for bottom vgg layers
        if not end_to_end:
            freeze_first_n_layers(ssd_net.vgg, 21)

        params = filter(lambda p: p.requires_grad, net.parameters())

        optimizer = torch.optim.SGD(params, lr=cfg['lr_list'][0], momentum=args.momentum,
                                    weight_decay=args.weight_decay)

        lr_scheduler = CyclicLRScheduler(optimizer=optimizer, upper_bound_steps=cfg['lr_steps'], lr_list=cfg['lr_list'])
    else:
        optimizer = None
        lr_scheduler = None

    criterion = MultiBoxLoss(num_classes=cfg['num_classes'],
                             overlap_thresh=cfg['multi_box_loss_overlap_thresh'], prior_for_matching=True, bkg_label=cfg['background_label'],
                             neg_mining=True, neg_pos=cfg['negative_positive_ratio'], neg_overlap=0.5,
                             encode_target=False, variance=cfg['variance'], use_gpu=use_cudnn)

    if not training:
        ssd_net.eval()

    net_components = namedtuple('SSDComponents', ['ssd', 'data_parallel', 'decode_boxes', 'softmax', 'optimizer', 'lr_scheduler', 'loss'])

    return net_components(ssd_net, net, decode_boxes, softmax, optimizer, lr_scheduler, criterion)
