from allegroai import InputModel, DataView

from dog_train import SSDExperiment

if __name__ == '__main__':

    #todo: can we get model by name?
    input_model = InputModel(model_id='f329c4b62823408d8c288312b97e0966') #Pytorch SSD on KITTI

    train_dataview = DataView()
    train_dataview.add_query(
        dataset_name='COCO - Common Objects in Context',
        version_name='Train2017 version',
        roi_query='person'
    )

    # set label mapping
    label_mapping = {'background': 0, 'person': 1}

    # override args
    args_override = dict(
        test_dataview_id="3a2bb3b0e0eb47f2b8d67554435d4bcc",
        batch_size=20,
        test_before_train=False,
        upload_destination="s3://deepx"
    )

    # todo how to override network_design and have it saved in task?
    net_design_override = None

    SSDExperiment(
        project_name='pytorch ssd',
        task_name='Train SSD modified example',
        args_override=args_override,
        train_dataview=train_dataview,
        net_design_override=net_design_override,
        label_mapping=label_mapping,
        input_model=input_model
    ).run()
